//
//  Constants.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

struct Constants {
  static let defaultAnimationDuration: NSTimeInterval = 0.5
  static let defaultAnimationDelay: NSTimeInterval = 0
  static let defaultAnimationDamping: CGFloat = 0.8
  static let animationNoDamping: CGFloat = 1
  static let defaultAnimationInitialVelocity: CGFloat = 1
  static let minimumPhoneLength: Int = 10
  static let maximumPhoneLength: Int = 11
  static let topViewHeight: CGFloat = 82
  static let tourViewButtonHeight: CGFloat = 66
  static let navigationBarHeight: CGFloat = 82
  static let tourDismissalTransitionDuration: CGFloat = 0.8
  
  static let dropProImageViewNotificationName = "DropProImageViewNotificationName"
  static let tourDidFinishNotificationName = "TourDidFinishNotificationName"
  static let hasPresentedTourUserDefaultsKey = "HasPresentedTourUserDefaultsKey"
  
  static let answersPageViewLogin = "iOS;iu.pro;Login"
  static let answersPageViewSignup = "iOS;iu.pro;Signup"
  static let answersPageViewHome = "iOS;iu.pro;Home"
  static let answersPageViewNewStudent = "iOS;iu.pro;New Student"
  static let answersPageViewBeforeAfter = "iOS;iu.pro;BeforeAfter"
  static let answersPageViewStudent = "iOS;iu.pro;Student"
  static let answersPageViewCalendar = "iOS;iu.pro;Calendar"
  static let answersPageViewHistory = "iOS;iu.pro;History"
  static let answersPageViewInvite = "iOS;iu.pro;Invite"
  static let answersPageViewTour = "iOS;iu.pro;Tour"
  
  static let appStoreLink = "https://itunes.apple.com/us/app/iu./id1086122688?ls=1&mt=8"
  static let iuAppStoreLink = "https://itunes.apple.com/us/app/iu./id1081967886?ls=1&mt=8"
  static let versionCheckerLink = "http://www.iuapp.co/api/v1/config.json"
}
