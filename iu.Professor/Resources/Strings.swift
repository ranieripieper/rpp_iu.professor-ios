//
//  Strings.swift
//  Iu
//
//  Created by Gilson Gil on 2/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct Strings {
  static let NetworkError = NSLocalizedString("NETWORK_ERROR", comment: "Login network error")
  
  static let TourNextButtonTitle = NSLocalizedString("TOUR_NEXTBUTTONTITLE", comment: "Tour next button title")
  static let TourLastButtonTitle = NSLocalizedString("TOUR_LASTBUTTONTITLE", comment: "Tour last button title")
  static let Tour1HelloLabelText = NSLocalizedString("TOUR_1_HELLOLABEL", comment: "Tour 1 hello label text")
  static let Tour1WelcomeLabelText = NSLocalizedString("TOUR_1_WELCOMELABEL", comment: "Tour 1 welcome label text")
  static let Tour1DescriptionLabelText = NSLocalizedString("TOUR_1_DESCRIPTION", comment: "Tour 1 description label text")
  static let Tour2ListLabelText = NSLocalizedString("TOUR_2_LIST", comment: "Tour 2 list label text")
  static let Tour2CreateLabelText = NSLocalizedString("TOUR_2_CREATE", comment: "Tour 2 create label text")
  static let Tour2NameLabelText = NSLocalizedString("TOUR_2_NAME", comment: "Tour 2 name label text")
  static let Tour3HowLabelText = NSLocalizedString("TOUR_3_HOW", comment: "Tour 3 how label text")
  static let Tour3BeforeAfterLabelText = NSLocalizedString("TOUR_3_BEFOREAFTER", comment: "Tour 3 beforeafter label text")
  static let Tour3DefineLabelText = NSLocalizedString("TOUR_3_DEFINE", comment: "Tour 3 define label text")
  static let Tour4ClassGivenLabelText = NSLocalizedString("TOUR_4_CLASSGIVEN", comment: "Tour 4 class given label text")
  static let Tour4AfterLabelText = NSLocalizedString("TOUR_4_AFTER", comment: "Tour 4 after label text")
  static let Tour4BeforeLabelText = NSLocalizedString("TOUR_4_BEFORE", comment: "Tour 4 before label text")
  static let Tour5DateLabelText = NSLocalizedString("TOUR_5_DATE", comment: "Tour 5 date label text")
  static let Tour5CalendarLabelText = NSLocalizedString("TOUR_5_CALENDAR", comment: "Tour 5 calendar label text")
  static let Tour5StudentLabelText = NSLocalizedString("TOUR_5_STUDENT", comment: "Tour 5 student label text")
  static let Tour6ByeLabelText = NSLocalizedString("TOUR_6_BYE", comment: "Tour 6 bye label text")
  
  static let LoginTypeEmail = NSLocalizedString("LOGIN_TYPEEMAIL", comment: "Type email placeholder")
  static let LoginTypePassword = NSLocalizedString("LOGIN_TYPEPASSWORD", comment: "Type password placeholder")
  static let LoginButton = NSLocalizedString("LOGIN_BUTTON", comment: "Login button label")
  static let LoginSignupLabel = NSLocalizedString("LOGIN_SIGNUPLABEL", comment: "Login sign up label")
  static let LoginSignupButton = NSLocalizedString("LOGIN_SIGNUPBUTTON", comment: "Login sign up button")
  static let LoginForgotLabel = NSLocalizedString("LOGIN_FORGOTLABEL", comment: "Login forgot label")
  static let LoginForgotButton = NSLocalizedString("LOGIN_FORGOTBUTTON", comment: "Login forgot button")
  static let LoginNotFoundError = NSLocalizedString("LOGIN_NOTFOUND_ERROR", comment: "Login user not found error message")
  
  static let SignupTypeName = NSLocalizedString("SIGNUP_TYPENAME", comment: "Type name placeholder")
  static let SignupTypeEmail = NSLocalizedString("SIGNUP_TYPEEMAIL", comment: "Type email placeholder")
  static let SignupTypePassword = NSLocalizedString("SIGNUP_TYPEPASSWORD", comment: "Type password placeholder")
  static let SignupTypePasswordConfirmation = NSLocalizedString("SIGNUP_TYPEPASSWORDCONFIRMATION", comment: "Type password confirmation placeholder")
  static let SignupButton = NSLocalizedString("SIGNUP_BUTTON", comment: "Signup button label")
  static let SignupEmailError = NSLocalizedString("SIGNUP_ERROREMAIL", comment: "Signup email in use error message")
  static let SignupPasswordMismatchError = NSLocalizedString("SIGNUP_ERRORPASSWORDMISMATCH", comment: "Signup password mismatch error message")
  
  static let HomeWelcomeLabel = NSLocalizedString("HOME_WELCOMELABEL", comment: "Welcome label")
  static let HomeStartButton = NSLocalizedString("HOME_STARTBUTTON", comment: "Welcome start button title")
  static let HomeDeleteAlert = NSLocalizedString("HOME_DELETEALERT", comment: "Delete alert label text")
  static let HomeDeleteAlertDelete = NSLocalizedString("HOME_DELETEALERTDELETE", comment: "Delete alert delete button title")
  static let HomeDeleteAlertCancel = NSLocalizedString("HOME_DELETEALERTCANCEL", comment: "Delete alert cancel button title")
  
  static let NewStudentTypeName = NSLocalizedString("NEWSTUDENT_TYPENAME", comment: "Type name placeholder")
  static let NewStudentTypePhone = NSLocalizedString("NEWSTUDENT_TYPEPHONE", comment: "Type phone placeholder")
  static let NewStudentNextButtonTitle = NSLocalizedString("NEWSTUDENT_BUTTONTITLE", comment: "Next button title")
  static let NewStudentExistingNumberErrorAlert = NSLocalizedString("NEWSTUDENT_EXISTINGNUMBERERRORALERT", comment: "Existing number error alert message")
  
  static let StudentClassCountBefore = NSLocalizedString("STUDENT_CLASSCOUNTBEFORELABEL", comment: "Classes count before label")
  static let StudentClassExpire = NSLocalizedString("STUDENT_CLASSEXPIRELABEL", comment: "Classes expire label")
  static let StudentClassCountAfter = NSLocalizedString("STUDENT_CLASSCOUNTAFTERLABEL", comment: "Classes count after label")
  static let StudentClassPayment = NSLocalizedString("STUDENT_CLASSPAYMENTLABEL", comment: "Classes expire label")
  static let StudentSaveButtonTitle = NSLocalizedString("STUDENT_SAVEBUTTONTITLE", comment: "Save button title")
  
  static let DatePickerOkButtonTitle = NSLocalizedString("DATEPICKER_OK", comment: "Date picker ok button title")
  
  static let PaymentTypeTopLabel = NSLocalizedString("PAYMENTTYPE_TOPLABEL", comment: "Payment type top label text")
  static let PaymentTypeBeforeButton = NSLocalizedString("PAYMENTTYPE_BEFOREBUTTON", comment: "Payment type before button title")
  static let PaymentTypeBeforeLabel = NSLocalizedString("PAYMENTTYPE_BEFORELABEL", comment: "Payment type before label text")
  static let PaymentTypeAfterButton = NSLocalizedString("PAYMENTTYPE_AFTERBUTTON", comment: "Payment type after button title")
  static let PaymentTypeAfterLabel = NSLocalizedString("PAYMENTTYPE_AFTERLABEL", comment: "Payment type after label text")
  static let PaymentTypeNextButton = NSLocalizedString("PAYMENTTYPE_NEXTBUTTON", comment: "Payment type next button title")
  
  static let ClassesAvailableClasses = NSLocalizedString("CLASSES_AVAILABLE_CLASSES", comment: "Available classes label")
  static let ClassesClass = NSLocalizedString("CLASSES_CLASS", comment: "Class label")
  static let ClassesClasses = NSLocalizedString("CLASSES_CLASSES", comment: "Classes label")
  static let ClassesNoClasses = NSLocalizedString("CLASSES_NOCLASSES", comment: "No classes label")
  static let ClassesLogout = NSLocalizedString("CLASSES_LOGOUT", comment: "Logout label")
  static let ClassesGiven = NSLocalizedString("CLASSES_CLASSESGIVEN", comment: "Classes given label")
  static let ClassesSeeMore = NSLocalizedString("CLASSES_SEEMORE", comment: "See more label")
  
  static let UseClassesTitle = NSLocalizedString("USECLASSES_TITLE", comment: "Use Classes title label")
  static let UseClassesOkButtonTitle = NSLocalizedString("USECLASSES_OKBUTTONTITLE", comment: "Ok Button title")
  static let UseClassesSaveButtonTitle = NSLocalizedString("USECLASSES_SAVEBUTTONTITLE", comment: "Save Button title")
  
  static let InviteLabel = NSLocalizedString("INVITE_LABEL", comment: "Invite label text")
  static let InviteButton = NSLocalizedString("INVITE_INVITEBUTTON", comment: "Invite button title")
  static let InviteLaterButton = NSLocalizedString("INVITE_LATER", comment: "Invite later button title")
  static let InviteShare = NSLocalizedString("INVITE_SHARETEXT", comment: "Invite share text")
  
  static let AlertVersionLabel = NSLocalizedString("ALERT_VERSIONLABEL", comment: "Alert version label text")
  static let AlertUpdateButton = NSLocalizedString("ALERT_UPDATEBUTTON", comment: "Alert update button title")
  static let AlertUpdateNowButton = NSLocalizedString("ALERT_UPDATENOWBUTTON", comment: "Alert update now button title")
  static let AlertCancelButton = NSLocalizedString("ALERT_CANCELBUTTON", comment: "Alert cancel button title")
}
