//
//  UIFont+iu.swift
//  Iu
//
//  Created by Gilson Gil on 2/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

extension UIFont {
  static func iuBlackFont(size: CGFloat) -> UIFont? {
    return UIFont(name: "Avenir-Black", size: size)
  }
  
  static func iuHeavyFont(size: CGFloat) -> UIFont? {
    return UIFont(name: "Avenir-Heavy", size: size)
  }
}
