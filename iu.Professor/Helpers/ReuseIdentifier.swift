//
//  ReuseIdentifier.swift
//  Classes-Instructor
//
//  Created by Gilson Gil on 1/21/16.
//  Copyright © 2016 gtakaasig. All rights reserved.
//

import Foundation

protocol ReuseIdentifier {
  static func reuseIdentifier() -> String
}
