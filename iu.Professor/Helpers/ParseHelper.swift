//
//  ParseHelper.swift
//  Iu
//
//  Created by Gilson Gil on 2/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

enum ParseError: ErrorType {
  case Unhandled
  case NotFound
  case Network
  
  func description() -> String {
    switch self {
    case .Unhandled:
      return "Erro desconhecido"
    case .NotFound:
      return Strings.LoginNotFoundError
    case .Network:
      return Strings.NetworkError
    }
  }
}

struct ParseHelper {
  static func register() {
    Student.registerSubclass()
    Parse.enableLocalDatastore()
    guard let dict = NSBundle.mainBundle().infoDictionary?["Parse"] as? [String: String], applicationId = dict["ApplicationId"], clientKey = dict["ClientKey"] else {
      return
    }
    Parse.setApplicationId(applicationId, clientKey: clientKey)
  }
  
  static let apiDateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    dateFormatter.dateFormat = "yyyy-MM-dd"
    return dateFormatter
  }()
  
  static func isLoggedIn() -> Bool {
    if let _ = PFUser.currentUser() {
      return true
    } else {
      return false
    }
  }
  
  static func currentUserName() -> String {
    if let user = PFUser.currentUser() {
      return user["name"] as? String ?? ""
    } else {
      return ""
    }
  }
  
  static func signup(name: String, email: String, password: String, completion: (inner: () throws -> ()) -> ()) {
    let user = PFUser()
    user["name"] = name
    user.username = email
    user.email = email.lowercaseString
    user.password = password
    user.signUpInBackgroundWithBlock { success, error in
      AnswersHelper.signUp(success)
      guard success else {
        completion { throw ParseError.Unhandled }
        return
      }
      completion { return }
    }
  }
  
  static func login(email: String, password: String, completion: (inner: () throws -> ()) -> ()) {
    PFUser.logInWithUsernameInBackground(email.lowercaseString, password: password) { user, error in
      let success = error == nil
      AnswersHelper.signIn(success)
      guard success else {
        if error!.code == 101 {
          completion { throw ParseError.NotFound }
          return
        }
        completion { throw ParseError.Unhandled }
        return
      }
      completion { return }
    }
  }
  
  static func forgotPassword(email: String, completion: (inner: () throws -> ()) -> ()) {
    PFUser.requestPasswordResetForEmailInBackground(email) { success, error in
      AnswersHelper.forgotPassword()
      guard error == nil else {
        completion {throw LoginError.EmailNotFound}
        return
      }
      completion {return}
    }
  }
  
  static func fetchStudents(completion: (inner: () throws -> [Student]) -> ()) {
    guard let user = PFUser.currentUser() else {
      completion { throw ParseError.Unhandled }
      return
    }
    
    guard let query = Student.query() else {
      completion { throw ParseError.Unhandled }
      return
    }
    query.whereKey("instructors", equalTo: user)
    query.findObjectsInBackgroundWithBlock { (students: [PFObject]?, error: NSError?) -> Void in
      guard error == nil else {
        completion { throw ParseError.NotFound }
        return
      }
      
      guard let students = students as? [Student] else {
        completion { throw ParseError.NotFound }
        return
      }
      
      completion { return students }
    }
  }
  
  static func deleteStudent(student: Student, completion: () -> ()) {
    guard let user = PFUser.currentUser() else {
      completion()
      return
    }
    let instructorRelation = student.relationForKey("instructors")
    instructorRelation.removeObject(user)
    student.saveInBackgroundWithBlock { success, error in
      if success {
        AnswersHelper.deleteStudent(student.objectId, studentPhone: student["phone"] as? Double)
      }
      completion()
    }
  }
  
  static func addStudent(student: Student, completion: (inner: () throws -> Student) -> ()) {
    guard let user = PFUser.currentUser(), objectId = user.objectId, name = student["name"] as? String, phone = student["phone"] as? Double else {
      completion {throw ParseError.Unhandled}
      return
    }
    
    PFCloud.callFunctionInBackground("students_add", withParameters: ["student_name": name, "student_phone": phone, "teacher_id": objectId]) { data, error in
      guard error == nil else {
        completion { throw ParseError.Unhandled }
        return
      }
      guard let createdStudent = data as? Student else {
        completion { throw ParseError.Unhandled }
        return
      }
      completion { return createdStudent }
    }
  }
  
  static func givenClasses(phone: Double, page: Int, completion: (inner: () throws -> [NSDate]) -> ()) {
    guard let user = PFUser.currentUser(), objectId = user.objectId else {
      completion { throw ParseError.Unhandled }
      return
    }
    
    let phoneString = String(format: "%.0f", arguments: [phone as? Double ?? 0])
    
    
    PFCloud.callFunctionInBackground("classes_get_done", withParameters: ["phone_number": phoneString, "teacher_id": objectId, "page": page]) { data, error in
      guard error == nil else {
        completion { throw ParseError.Unhandled }
        return
      }
      guard let data = data as? [PFObject] else {
        completion { throw ParseError.Unhandled }
        return
      }
      let dates = data.flatMap { dataObject -> NSDate? in
        dataObject["usedAt"] as? NSDate
      }
      completion { return dates }
    }
  }
  
  static func addClassesPayBefore(studentId: String, amount: Int, expirationDate: NSDate, completion: (inner: () throws -> ()) -> ()) {
    guard let user = PFUser.currentUser(), objectId = user.objectId else {
      completion {throw ParseError.Unhandled}
      return
    }
    
    let dateString = apiDateFormatter.stringFromDate(expirationDate)
    PFCloud.callFunctionInBackground("classes_pay_before_add_classes", withParameters: ["qt_classes": amount, "teacher_id": objectId, "student_id": studentId, "dt_validade": dateString]) { data, error in
      guard error == nil else {
        print(error)
        completion { throw ParseError.Unhandled }
        return
      }
      completion { return }
    }
  }
  
  static func payBeforeCompleteClasses(studentId: String, dates: [NSDate], date: NSDate, completion: (inner: () throws -> ()) -> ()) {
    guard let user = PFUser.currentUser(), objectId = user.objectId else {
      completion {throw ParseError.Unhandled}
      return
    }
    let dateStrings = dates.flatMap {
      apiDateFormatter.stringFromDate($0)
    }
    let dateString = apiDateFormatter.stringFromDate(date)
    PFCloud.callFunctionInBackground("classes_pay_before_set_done_classes", withParameters: ["teacher_id": objectId, "student_id": studentId, "dates": dateStrings, "expired_at": dateString]) { data, error in
      guard error == nil else {
        completion { throw ParseError.Unhandled }
        return
      }
      completion { return }
    }
  }
  
  static func addClassesPayAfter(studentId: String, paymentDate: NSDate, dates: [NSDate], completion: (inner: () throws -> ()) -> ()) {
    guard let user = PFUser.currentUser(), objectId = user.objectId else {
      completion {throw ParseError.Unhandled}
      return
    }
    
    let dateString = apiDateFormatter.stringFromDate(paymentDate)
    let dateStrings = dates.flatMap {
      apiDateFormatter.stringFromDate($0)
    }
    PFCloud.callFunctionInBackground("classes_pay_after_add_classes", withParameters: ["dates": dateStrings, "teacher_id": objectId, "student_id": studentId, "pay_at": dateString]) { data, error in
      guard error == nil else {
        print(error)
        completion { throw ParseError.Unhandled }
        return
      }
      completion { return }
    }
  }
  
  static func payAfterPay(studentId: String, amount: Int, payedAt: NSDate, completion: (inner: () throws -> ()) -> ()) {
    guard let user = PFUser.currentUser(), objectId = user.objectId else {
      completion {throw ParseError.Unhandled}
      return
    }
    
    let dateString = apiDateFormatter.stringFromDate(payedAt)
    PFCloud.callFunctionInBackground("classes_pay_after_update_to_paid", withParameters: ["qt_classes": amount, "teacher_id": objectId, "student_id": studentId, "pay_at": dateString]) { data, error in
      guard error == nil else {
        print(error)
        completion { throw ParseError.Unhandled }
        return
      }
      completion { return }
    }
  }
  
  static func payAfterUpdatePaymentDate(studentId: String, paymentDate: NSDate, completion: (inner: () throws -> ()) -> ()) {
    guard let user = PFUser.currentUser(), objectId = user.objectId else {
      completion {throw ParseError.Unhandled}
      return
    }
    
    let dateString = apiDateFormatter.stringFromDate(paymentDate)
    PFCloud.callFunctionInBackground("classes_pay_after_update_dt_pay", withParameters: ["teacher_id": objectId, "student_id": studentId, "pay_at": dateString]) { data, error in
      guard error == nil else {
        print(error)
        completion { throw ParseError.Unhandled }
        return
      }
      completion { return }
    }
  }
  
  static func getStudentSummary(phone: String, completion: (inner: () throws -> [String: AnyObject]) -> ()) {
    guard let user = PFUser.currentUser(), objectId = user.objectId else {
      completion {throw ParseError.Unhandled}
      return
    }
    
    PFCloud.callFunctionInBackground("student_get_summary", withParameters: ["teacher_id": objectId, "phone_number": phone]) { data, error in
      guard error == nil else {
        print(error)
        completion { throw ParseError.Unhandled }
        return
      }
      guard let data = data as? [String: AnyObject] else {
        completion { throw ParseError.Unhandled }
        return
      }
      completion { return data }
    }
  }
  
  static func signout() {
    PFUser.logOutInBackground()
    AnswersHelper.signOut()
  }
}
