//
//  UIImage+iu.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

extension UIImage {
  static func iuLogo() -> UIImage? {
    return UIImage(named: "imgLogo")
  }
  
  static func iuOps() -> UIImage? {
    return UIImage(named: "imgOps")
  }
  
  static func iuDelete() -> UIImage? {
    return UIImage(named: "icnDelete")
  }
  
  static func iuLess() -> UIImage? {
    return UIImage(named: "btnLess")
  }
  
  static func iuMore() -> UIImage? {
    return UIImage(named: "btnMore")
  }
  
  static func iuPro() -> UIImage? {
    return UIImage(named: "imgPro")
  }
  
  static func iuEnvelop1() -> UIImage? {
    return UIImage(named: "imgEnvelop1")
  }
  
  static func iuEnvelop2() -> UIImage? {
    return UIImage(named: "imgEnvelop2")
  }
  
  static func iuEnvelop3() -> UIImage? {
    return UIImage(named: "imgEnvelop3")
  }
  
  static func iuEnvelop4() -> UIImage? {
    return UIImage(named: "imgEnvelop4")
  }
  
  static func iuShadow() -> UIImage? {
    return UIImage(named: "imgShadow")
  }
  
  static func iuDevice() -> UIImage? {
    return UIImage(named: "imgDevice")
  }
  
  static func iuTourList1() -> UIImage? {
    return UIImage(named: "imgTourList1")
  }
  
  static func iuTourList2() -> UIImage? {
    return UIImage(named: "imgTourList2")
  }
  
  static func iuTourList3() -> UIImage? {
    return UIImage(named: "imgTourList3")
  }
  
  static func iuTourListImages() -> [UIImage] {
    var tourImages = [UIImage]()
    if let image1 = iuTourList1() {
      tourImages.append(image1)
    }
    if let image2 = iuTourList2() {
      tourImages.append(image2)
    }
    if let image3 = iuTourList3() {
      tourImages.append(image3)
    }
    return tourImages
  }
  
  static func iuHand() -> UIImage? {
    return UIImage(named: "imgHand")
  }
  
  static func iuDeviceBefore() -> UIImage? {
    return UIImage(named: "imgDeviceAntes")
  }
  
  static func iuDeviceAfter() -> UIImage? {
    return UIImage(named: "imgDeviceDepois")
  }
  
  static func iuTourLess() -> UIImage? {
    return UIImage(named: "imgTourLess")
  }
  
  static func iuTourMore() -> UIImage? {
    return UIImage(named: "imgTourMore")
  }
  
  static func iuCalendar() -> UIImage? {
    return UIImage(named: "imgCalendar")
  }
  
  static func iuSelected() -> UIImage? {
    return UIImage(named: "imgSelected")
  }
}
