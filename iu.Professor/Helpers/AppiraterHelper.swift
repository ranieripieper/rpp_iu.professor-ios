//
//  AppiraterHelper.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/17/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct AppiraterHelper {
  static func register() {
    Appirater.setAppId("1086122688")
    Appirater.setDaysUntilPrompt(7)
    Appirater.setUsesUntilPrompt(10)
    Appirater.setSignificantEventsUntilPrompt(-1)
    Appirater.setTimeBeforeReminding(2)
    Appirater.setDebug(false)
    Appirater.appLaunched(true)
  }
  
  static func resume() {
    Appirater.appEnteredForeground(true)
  }
}
