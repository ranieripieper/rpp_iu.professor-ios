//
//  String.swift
//  iu.Professor
//
//  Created by Gilson Gil on 4/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

extension String {
  mutating func stripEndingSpaces() -> String {
    while self.substringFromIndex(endIndex.advancedBy(-1)) == " " {
      removeAtIndex(endIndex.advancedBy(-1))
    }
    return self
  }
  
  func phoneStrip() -> String {
    let strippedPhone = self
      .stringByReplacingOccurrencesOfString(" ", withString: "")
      .stringByReplacingOccurrencesOfString("-", withString: "")
      .stringByReplacingOccurrencesOfString("(", withString: "")
      .stringByReplacingOccurrencesOfString(")", withString: "")
    
    return strippedPhone
  }
}
