//
//  AnswersHelper.swift
//  iu.Professor
//
//  Created by Gilson Gil on 5/1/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Crashlytics

struct AnswersHelper {
  static func signUp(success: Bool) {
    #if RELEASE
      Answers.logSignUpWithMethod(nil, success: success, customAttributes: nil)
    #endif
  }
  
  static func signIn(success: Bool) {
    #if RELEASE
      Answers.logLoginWithMethod(nil, success: success, customAttributes: nil)
    #endif
  }
  
  static func signOut() {
    #if RELEASE
      Answers.logCustomEventWithName("Sign out", customAttributes: nil)
    #endif
  }
  
  static func invite(media: String?) {
    #if RELEASE
      var attributes: [String: AnyObject] = ["ProInvited": true]
      if let media = media {
        attributes["Media"] = media
      }
      Answers.logInviteWithMethod(nil, customAttributes: attributes)
    #endif
  }
  
  static func pageView(pageName: String) {
    #if RELEASE
      Answers.logContentViewWithName(pageName, contentType: "View", contentId: nil, customAttributes: nil)
    #endif
  }
  
  static func forgotPassword() {
    #if RELEASE
      Answers.logCustomEventWithName("Forgot Password", customAttributes: nil)
    #endif
  }
  
  static func deleteStudent(studentId: String?, studentPhone: Double?) {
    #if RELEASE
      guard let studentId = studentId, studentPhone = studentPhone else {
        return
      }
      Answers.logCustomEventWithName("Delete Student", customAttributes: ["Student Id": studentId, "Student Phone": studentPhone])
    #endif
  }
  
  static func addStudent(studentPhone: Double?) {
    #if RELEASE
      guard let studentPhone = studentPhone else {
        return
      }
      Answers.logCustomEventWithName("Add Student", customAttributes: ["Student Phone": studentPhone])
    #endif
  }
  
  static func choseBefore() {
    #if RELEASE
      Answers.logCustomEventWithName("PayBefore", customAttributes: nil)
    #endif
  }
  
  static func choseAfter() {
    #if RELEASE
      Answers.logCustomEventWithName("PayAfter", customAttributes: nil)
    #endif
  }
}
