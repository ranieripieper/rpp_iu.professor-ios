//
//  UITableView+Register.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/23/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

extension UITableView {
  func registerClass(anyClass: AnyClass) {
    registerClass(anyClass, forCellReuseIdentifier: NSStringFromClass(anyClass))
  }
}
