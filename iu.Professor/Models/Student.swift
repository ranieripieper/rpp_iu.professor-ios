//
//  Student.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

class Student: PFObject {
  
}

extension Student: PFSubclassing {
  static func parseClassName() -> String {
    return "Student"
  }
}
