//
//  SignupViewController.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class SignupViewController: UIViewController {
  private let signupView = SignupView()
  
  weak var navigationDelegate: NavigationControllerDelegate?
  
  override func loadView() {
    view = signupView
    signupView.delegate = self
    AnswersHelper.pageView(Constants.answersPageViewSignup)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let keyboardHandler = KeyboardHandlerViewController(scrollView: signupView.scrollView)
    addChildViewController(keyboardHandler)
    keyboardHandler.didMoveToParentViewController(self)
    view.addSubview(keyboardHandler.view)
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension SignupViewController: SignupViewDelegate {
  func signupViewDidRegister(signupView: SignupView?) {
    navigationDelegate?.goToHome()
  }
}

extension SignupViewController: NavigationControllerProtocol {}
