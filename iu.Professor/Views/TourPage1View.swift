//
//  TourPage1View.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TourPage1View: UIView, TourPageViewProtocol {
  private let helloLabel = UILabel()
  private let welcomeLabel = UILabel()
  private let phoneImageView = PhoneImageView()
  private let shadowImageView = UIImageView()
  private let descriptionLabel = UILabel()
  
  var labels: [UIView] = []
  var images: [UIView] = []
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.iuDarkGreyColor()
    
    helloLabel.translatesAutoresizingMaskIntoConstraints = false
    helloLabel.font = UIFont.iuBlackFont(36)
    helloLabel.text = Strings.Tour1HelloLabelText
    helloLabel.textColor = UIColor.whiteColor()
    helloLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(helloLabel)
    
    welcomeLabel.translatesAutoresizingMaskIntoConstraints = false
    welcomeLabel.font = UIFont.iuBlackFont(14)
    welcomeLabel.text = Strings.Tour1WelcomeLabelText
    welcomeLabel.textColor = UIColor.whiteColor()
    welcomeLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(welcomeLabel)
    
    phoneImageView.translatesAutoresizingMaskIntoConstraints = false
    phoneImageView.setContentCompressionResistancePriority(UILayoutPriorityDefaultHigh, forAxis: .Vertical)
    phoneImageView.contentMode = .ScaleAspectFit
    addSubview(phoneImageView)
    
    shadowImageView.translatesAutoresizingMaskIntoConstraints = false
    shadowImageView.image = UIImage.iuShadow()
    shadowImageView.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    shadowImageView.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(shadowImageView)
    
    descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    descriptionLabel.font = UIFont.iuBlackFont(14)
    descriptionLabel.text = Strings.Tour1DescriptionLabelText
    descriptionLabel.textColor = UIColor.whiteColor()
    descriptionLabel.textAlignment = .Center
    descriptionLabel.numberOfLines = 0
    descriptionLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(descriptionLabel)
    
    labels = [helloLabel, welcomeLabel, descriptionLabel]
    images = [phoneImageView, shadowImageView]
    
    let metrics = ["margin": 20]
    let views = ["helloLabel": helloLabel, "welcomeLabel": welcomeLabel, "phoneImageView": phoneImageView, "shadowImageView": shadowImageView, "descriptionLabel": descriptionLabel]
    views.forEach {
      let horizontalConstraints = [
        NSLayoutConstraint(item: $0.1, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: $0.1, attribute: .Left, relatedBy: .GreaterThanOrEqual, toItem: self, attribute: .LeftMargin, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: $0.1, attribute: .Right, relatedBy: .LessThanOrEqual, toItem: self, attribute: .RightMargin, multiplier: 1, constant: 0)
      ]
      addConstraints(horizontalConstraints)
    }
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-[helloLabel]-margin-[welcomeLabel]-margin-[phoneImageView]-[shadowImageView]-margin-[descriptionLabel]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    let centerConstraint = NSLayoutConstraint(item: phoneImageView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: Constants.tourViewButtonHeight / 2)
    addConstraint(centerConstraint)
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    fadeIn(nil)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Private
private extension TourPage1View {
  func fadeIn(completion: (() -> ())?) {
    helloLabel.alpha = 0
    welcomeLabel.alpha = 0
    phoneImageView.alpha = 0
    shadowImageView.alpha = 0
    descriptionLabel.alpha = 0
    UIView.animateWithDuration(1) { [weak self] in
      self?.helloLabel.alpha = 1
      self?.welcomeLabel.alpha = 1
      self?.phoneImageView.alpha = 1
      self?.shadowImageView.alpha = 1
      self?.descriptionLabel.alpha = 1
    }
  }
}

final class PhoneImageView: UIImageView, WiggleProtocol {
  private var timer: NSTimer?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    
    image = UIImage.iuDevice()
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(1 * NSEC_PER_SEC)), dispatch_get_main_queue()) { [weak self] in
      if let weakSelf = self {
        weakSelf.wiggle()
        weakSelf.timer = NSTimer.scheduledTimerWithTimeInterval(4, target: weakSelf, selector: #selector(PhoneImageView.aWiggle), userInfo: nil, repeats: true)
      }
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Public
extension PhoneImageView {
  func aWiggle() {
    wiggle()
  }
}
