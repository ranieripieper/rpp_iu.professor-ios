//
//  KeyboardHandlerViewController.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class KeyboardHandlerViewController: UIViewController {
  private let scrollView: UIScrollView!
  
  required init?(coder aDecoder: NSCoder) {
    scrollView = nil
    super.init(coder: aDecoder)
  }
  
  init(scrollView: UIScrollView) {
    self.scrollView = scrollView
    super.init(nibName: nil, bundle: nil)
    view.alpha = 0
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(KeyboardHandlerViewController.keyboardAppeared(_:)), name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(KeyboardHandlerViewController.keyboardDisappeared(_:)), name: UIKeyboardWillHideNotification, object: nil)
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
  }
  
  func keyboardAppeared(notification: NSNotification) {
    if let keyboardRect = notification.userInfo?[UIKeyboardFrameEndUserInfoKey]?.CGRectValue {
      let contentInsets = UIEdgeInsets(top: scrollView.contentInset.top, left: 0, bottom: keyboardRect.height, right: 0)
      scrollView.contentInset = contentInsets
      scrollView.scrollIndicatorInsets = contentInsets
    }
  }
  
  func keyboardDisappeared(notification: NSNotification) {
    UIView.animateWithDuration(Constants.defaultAnimationDuration) { [weak self] in
      self?.scrollView.contentInset = UIEdgeInsetsZero
      self?.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
