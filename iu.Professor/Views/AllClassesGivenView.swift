//
//  AllClassesGivenView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/30/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol AllClassesGivenViewDelegate: class {
  func pop()
}

final class AllClassesGivenView: UIView {
  private let margin: CGFloat = 10
  private let loadingViewSize: CGFloat = 60
  private let tableView = UITableView(frame: .zero, style: .Plain)
  private let loadingView = LoadingView()
  
  private var student: Student?
  private var allClassesGivenViewModel: AllClassesGivenViewModel
  private var nextPage: Int? = 1
  private var isLoading = false
  
  weak var delegate: AllClassesGivenViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(student: Student) {
    self.student = student
    allClassesGivenViewModel = AllClassesGivenViewModel(student: student)
    super.init(frame: .zero)
    setUp()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    if allClassesGivenViewModel.months.count == 0 && loadingView.superview == nil {
      startLoading()
    }
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.backgroundColor = UIColor.iuAlmostWhiteColor()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.estimatedRowHeight = 100
    tableView.separatorStyle = .None
    tableView.allowsSelection = false
    tableView.alpha = 0
    addSubview(tableView)
    
    let views = ["tableView": tableView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(verticalConstraints)
    
    load()
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Private
private extension AllClassesGivenView {
  func load() {
    guard let nextPage = nextPage where !isLoading else {
      return
    }
    isLoading = true
    allClassesGivenViewModel.load(nextPage) { [weak self] inner in
      do {
        self?.allClassesGivenViewModel = try inner()
        self?.allClassesGivenViewModel.months.forEach {
          self?.tableView.registerClass($0.cellClass, forCellReuseIdentifier: $0.reuseIdentifier)
        }
        self?.tableView.reloadData()
        UIView.animateWithDuration(Constants.defaultAnimationDuration, animations: { [weak self] in
          self?.tableView.alpha = 1
          self?.loadingView.alpha = 0
          }, completion: { [weak self] _ in
            self?.stopLoading()
            if self?.nextPage != nil {
              self?.nextPage! += 1
            }
          })
      } catch {
        self?.nextPage = nil
        if self?.allClassesGivenViewModel.months.count == 0 {
          self?.delegate?.pop()
        }
      }
      self?.isLoading = false
    }
  }
  
  func startLoading() {
    loadingView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(loadingView)
    
    let horizontalConstraints = [
      NSLayoutConstraint(item: loadingView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: loadingView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: loadingViewSize)
    ]
    addConstraints(horizontalConstraints)
    let verticalConstraints = [
      NSLayoutConstraint(item: loadingView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: loadingView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: loadingViewSize)
    ]
    addConstraints(verticalConstraints)
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(5 * NSEC_PER_MSEC)), dispatch_get_main_queue()) { [weak self] in
      self?.loadingView.startAnimating()
    }
  }
  
  func stopLoading() {
    loadingView.endAnimating()
    loadingView.removeFromSuperview()
  }
}

// MARK: TableView DataSource
extension AllClassesGivenView: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return allClassesGivenViewModel.months.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let configurator = allClassesGivenViewModel.months[indexPath.row]
    let cell = tableView.dequeueReusableCellWithIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath)
    configurator.update(cell)
    return cell
  }
}

// MARK: TableView Delegate
extension AllClassesGivenView: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    guard let viewModel = allClassesGivenViewModel.months[indexPath.row].currentViewModel() as? AllClassesGivenCellViewModel else {
      return UITableViewAutomaticDimension
    }
    let lines = Int((viewModel.dates.count - 1) / 3) + 1
    return 40 + 60 + 20 * CGFloat(lines)
  }
}

// MARK: ScrollView Delegate
extension AllClassesGivenView: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView.contentOffset.y + scrollView.bounds.height > scrollView.contentSize.height {
      load()
    }
  }
}
