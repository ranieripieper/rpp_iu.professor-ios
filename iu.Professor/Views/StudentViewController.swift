//
//  StudentViewController.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/23/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class StudentViewController: UIViewController {
  private let studentView: StudentView
  private let studentViewModel: StudentViewModel
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(studentViewModel: StudentViewModel) {
    self.studentViewModel = studentViewModel
    studentView = StudentView(studentViewModel: studentViewModel)
    super.init(nibName: nil, bundle: nil)
    studentView.delegate = self
  }
  
  override func loadView() {
    view = studentView
    AnswersHelper.pageView(Constants.answersPageViewStudent)
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Student View Delegate
extension StudentViewController: StudentViewDelegate {
  func didSave() {
    navigationController?.popToRootViewControllerAnimated(true)
  }
  
  func invite(name: String) {
    let inviteViewController = InviteViewController(name: name)
    navigationController?.pushViewController(inviteViewController, animated: true)
  }
  
  func studentView(studentView: StudentView, classesCount: Int, useClassesCount: Int, expireDate: NSDate, newStudent: Student?) {
    var newStudentViewModel: StudentViewModel
    if let newStudent = newStudent {
      newStudentViewModel = StudentViewModel(student: newStudent)
      newStudentViewModel.paymentType = studentViewModel.paymentType
    } else {
      newStudentViewModel = studentViewModel
    }
    let useClassesViewController = UseClassesViewController(studentViewModel: newStudentViewModel, expireDate: expireDate, classesCount: classesCount, useClassesCount: useClassesCount)
    navigationController?.pushViewController(useClassesViewController, animated: true)
    
    let filteredViewControllers = navigationController?.viewControllers.filter {
      $0.isKindOfClass(HomeViewController.classForCoder()) || $0.isKindOfClass(StudentViewController.classForCoder()) || $0.isKindOfClass(UseClassesViewController.classForCoder())
    }
    guard let newViewControllers = filteredViewControllers where filteredViewControllers?.count > 1 else {
      return
    }
    navigationController?.viewControllers = newViewControllers
  }
  
  func showAllClassesGiven() {
    let allClassesGivenViewController = AllClassesGivenViewController(student: studentViewModel.student)
    navigationController?.pushViewController(allClassesGivenViewController, animated: true)
  }
}
