//
//  HomeView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol HomeViewDelegate: class {
  func addAddButtonToHomeViewModel(homeViewModel: HomeViewModel)
  func removeAddButtonToHomeViewModel(homeViewModel: HomeViewModel)
  func addStudent(existingPhones: [Double])
  func goToStudent(studentViewModel: StudentViewModel)
  func goToPaymentType(student: Student)
  func logoutPressedOnHomeView(homeView: HomeView)
}

final class HomeView: UIView {
  private let margin: CGFloat = 24
  private let textFieldPadding: CGFloat = 12
  private let logoutHeight: CGFloat = 72
  private let minimumLogoutLimit: CGFloat = 0.4
  private let logoutBackgroundOffAlpha: CGFloat = 0.6
  private let loadingViewSize: CGFloat = 60
  private let buttonHeight: CGFloat = 64
  private let padding: CGFloat = 20
  private let cellHeight: CGFloat = 80
  
  private let tableView = UITableView()
  private let logoutLabel = UILabel()
  private var currentNumberOfStudents: Int? = nil
  private let loadingView = LoadingView()
  private var isLoading = false
  
  var homeViewModel: HomeViewModel? {
    didSet {
      if let homeViewModel = homeViewModel {
        dispatch_async(dispatch_get_main_queue()) { [weak self] in
          self?.configure(homeViewModel)
          self?.stopLoading()
        }
      }
    }
  }
  weak var delegate: HomeViewDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    if homeViewModel == nil {
      startLoading()
    }
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    loadingView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(loadingView)
    
    let horizontalConstraints = [
      NSLayoutConstraint(item: loadingView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: loadingViewSize),
      NSLayoutConstraint(item: loadingView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0)
    ]
    addConstraints(horizontalConstraints)
    
    let verticalConstraints = [
      NSLayoutConstraint(item: loadingView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: loadingViewSize),
      NSLayoutConstraint(item: loadingView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    addConstraints(verticalConstraints)
  }
  
  func configure(homeViewModel: HomeViewModel) {
    if homeViewModel.students.count == 0 {
      if currentNumberOfStudents != 0 {
        subviews.forEach {
          if !($0 is LoadingView) {
            $0.removeFromSuperview()
          }
        }
        delegate?.removeAddButtonToHomeViewModel(homeViewModel)
        
        logoutLabel.translatesAutoresizingMaskIntoConstraints = false
        logoutLabel.font = UIFont.iuBlackFont(18)
        logoutLabel.text = Strings.ClassesLogout
        logoutLabel.textAlignment = .Center
        logoutLabel.textColor = UIColor.iuAlmostWhiteColor()
        logoutLabel.backgroundColor = UIColor.iuBlueyGreyColor()
        addSubview(logoutLabel)
        
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = UIColor.clearColor()
        scrollView.delegate = self
        addSubview(scrollView)
        
        let scrollViewContentView = UIView()
        scrollViewContentView.translatesAutoresizingMaskIntoConstraints = false
        scrollViewContentView.backgroundColor = UIColor.iuAlmostWhiteColor()
        scrollView.addSubview(scrollViewContentView)
        
        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        container.backgroundColor = UIColor.iuAlmostWhiteColor()
        scrollViewContentView.addSubview(container)
        
        let welcomeLabel = UILabel()
        welcomeLabel.translatesAutoresizingMaskIntoConstraints = false
        welcomeLabel.text = String(format: Strings.HomeWelcomeLabel, arguments: [ParseHelper.currentUserName()])
        welcomeLabel.textColor = UIColor.iuLightGreyColor()
        welcomeLabel.textAlignment = .Center
        welcomeLabel.font = UIFont.iuBlackFont(18)
        welcomeLabel.numberOfLines = 0
        container.addSubview(welcomeLabel)
        
        let button = UIButton(type: .Custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(Strings.HomeStartButton.uppercaseString, forState: .Normal)
        button.titleLabel?.textColor = UIColor.whiteColor()
        button.titleLabel?.font = UIFont.iuBlackFont(18)
        button.backgroundColor = UIColor.iuUglyPinkColor()
        button.addTarget(self, action: #selector(HomeView.startButtonPressed), forControlEvents: .TouchUpInside)
        button.layer.cornerRadius = 8
        container.addSubview(button)
        
        let views = ["container": container, "welcomeLabel": welcomeLabel, "button": button, "scrollView": scrollView, "scrollViewContentView": scrollViewContentView, "logoutLabel": logoutLabel]
        let metrics = ["buttonHeight": buttonHeight, "logoutHeight": logoutHeight, "padding": padding]
        var scrollViewHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollView]|", options: [], metrics: metrics, views: views)
        addConstraints(scrollViewHorizontalConstraints)
        
        scrollViewHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollViewContentView]|", options: [], metrics: metrics, views: views)
        scrollView.addConstraints(scrollViewHorizontalConstraints)
        
        scrollViewHorizontalConstraints = [NSLayoutConstraint(item: scrollViewContentView, attribute: .Width, relatedBy: .Equal, toItem: self, attribute: .Width, multiplier: 1, constant: 0)]
        addConstraints(scrollViewHorizontalConstraints)
        
        var scrollViewVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollView]|", options: [], metrics: metrics, views: views)
        addConstraints(scrollViewVerticalConstraints)
        
        scrollViewVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollViewContentView]|", options: [], metrics: metrics, views: views)
        scrollView.addConstraints(scrollViewVerticalConstraints)
        
        scrollViewVerticalConstraints = [NSLayoutConstraint(item: scrollViewContentView, attribute: .Height, relatedBy: .Equal, toItem: self, attribute: .Height, multiplier: 1, constant: 1)]
        addConstraints(scrollViewVerticalConstraints)
        
        var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[container]|", options: [], metrics: metrics, views: views)
        scrollViewContentView.addConstraints(horizontalConstraints)
        var verticalConstraints = [NSLayoutConstraint(item: container, attribute: .CenterY, relatedBy: .Equal, toItem: scrollViewContentView, attribute: .CenterY, multiplier: 1, constant: 0)]
        scrollViewContentView.addConstraints(verticalConstraints)
        
        horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[welcomeLabel]|", options: [], metrics: metrics, views: views)
        scrollViewContentView.addConstraints(horizontalConstraints)
        verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[welcomeLabel]-80-[button(buttonHeight)]|", options: [], metrics: metrics, views: views)
        scrollViewContentView.addConstraints(verticalConstraints)
        
        horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-padding-[button]-padding-|", options: [], metrics: metrics, views: views)
        scrollViewContentView.addConstraints(horizontalConstraints)
        
        let logoutXConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[logoutLabel]|", options: [], metrics: metrics, views: views)
        addConstraints(logoutXConstraints)
        
        let logoutYConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[logoutLabel(logoutHeight)]|", options: [], metrics: metrics, views: views)
        addConstraints(logoutYConstraints)
      }
    } else {
      if currentNumberOfStudents > 0 {
        if currentNumberOfStudents == homeViewModel.students.count {
          tableView.visibleCells.forEach {
            if let indexPath = tableView.indexPathForCell($0) {
              let configurator = homeViewModel.students[indexPath.row]
              configurator.update($0)
            }
          }
        } else {
          tableView.reloadData()
        }
      } else {
        subviews.forEach {
          if !($0 is LoadingView) {
            $0.removeFromSuperview()
          }
        }
        delegate?.addAddButtonToHomeViewModel(homeViewModel)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        logoutLabel.translatesAutoresizingMaskIntoConstraints = false
        logoutLabel.font = UIFont.iuBlackFont(18)
        logoutLabel.text = Strings.ClassesLogout
        logoutLabel.textAlignment = .Center
        logoutLabel.textColor = UIColor.iuAlmostWhiteColor()
        logoutLabel.backgroundColor = UIColor.iuBlueyGreyColor()
        addSubview(logoutLabel)
        
        tableView.backgroundColor = UIColor.clearColor()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 50
        tableView.separatorColor = UIColor.iuLightGreyColor()
        tableView.registerClass(HomeViewCell.classForCoder())
        tableView.registerClass(HomeViewEmptyCell.classForCoder())
        addSubview(tableView)
        
        let metrics = ["logoutHeight": logoutHeight]
        let views = ["tableView": tableView, "logoutLabel": logoutLabel]
        let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views)
        addConstraints(horizontalConstraints)
        
        let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: [], metrics: nil, views: views)
        addConstraints(verticalConstraints)
        
        let logoutXConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[logoutLabel]|", options: [], metrics: metrics, views: views)
        addConstraints(logoutXConstraints)
        
        let logoutYConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[logoutLabel(logoutHeight)]|", options: [], metrics: metrics, views: views)
        addConstraints(logoutYConstraints)
      }
    }
    currentNumberOfStudents = homeViewModel.students.count
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Private
private extension HomeView {
  func fetchStudent(student: Student) {
    guard !isLoading, let phone = student["phone"] as? Int else {
      return
    }
    
    let phoneString = String(format: "%.0f", arguments: [student["phone"] as? Double ?? 0])
    
    startLoading()
    homeViewModel?.getStudent(phoneString) { [weak self] inner in
      self?.stopLoading()
      do {
        let tuple = try inner()
        if tuple.0, let data = tuple.1 {
          guard let studentViewModel = StudentViewModel(student: student, data: data) else {
            return
          }
          self?.delegate?.goToStudent(studentViewModel)
        } else {
          self?.delegate?.goToPaymentType(student)
        }
      } catch {
        
      }
    }
  }
  
  func presentDeleteAlert(indexPath: NSIndexPath) {
    let buttons = [
      AlertButton(title: Strings.HomeDeleteAlertCancel, action: { [weak self] in
        self?.tableView.setEditing(false, animated: true)
        }),
      AlertButton(title: Strings.HomeDeleteAlertDelete, action: { [weak self] in
        self?.homeViewModel?.deleteStudentAtIndex(indexPath.row) {
          if let students = $0 {
            self?.homeViewModel?.students = students
            self?.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
          }
        }
        })
    ]
    AlertView.presentError(Strings.HomeDeleteAlert, buttons: buttons, inView: self)
  }
  
  func startLoading() {
    isLoading = true
    tableView.hidden = true
    logoutLabel.hidden = true
    loadingView.startAnimating()
  }
  
  func stopLoading() {
    isLoading = false
    tableView.hidden = false
    logoutLabel.hidden = false
    loadingView.endAnimating()
  }
}

// MARK: Actions
extension HomeView {
  func startButtonPressed() {
    guard !isLoading, let homeViewModel = homeViewModel else {
      return
    }
    
    delegate?.addStudent(homeViewModel.existingPhones())
  }
}

// MARK: TableView Data Source
extension HomeView: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return homeViewModel?.students.count ?? 0
    } else {
      return 1
    }
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if indexPath.section == 0 {
      return cellHeight
    } else {
      if let window = window {
        let possibleHeight = window.bounds.height - (convertPoint(tableView.frame.origin, toView: window).y + CGFloat(tableView.numberOfRowsInSection(0)) * cellHeight)
        if possibleHeight < 0 {
          return 0
        } else {
          return possibleHeight
        }
      }
      return 0
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if indexPath.section == 0 {
      guard let configurator = homeViewModel?.students[indexPath.row] else {
        return UITableViewCell()
      }
      let cell = tableView.dequeueReusableCellWithIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath)
      configurator.update(cell)
      return cell
    } else {
      guard let cell = tableView.dequeueReusableCellWithIdentifier(HomeViewEmptyCell.reuseIdentifier(), forIndexPath: indexPath) as? HomeViewEmptyCell else {
        return UITableViewCell()
      }
      return cell
    }
  }
}

// MARK: TableView Delegate
extension HomeView: UITableViewDelegate {
  func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    return true
  }
  
  func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
    return .Delete
  }
  
  func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
    let delete = UITableViewRowAction(style: .Destructive, title: "             ") { [weak self] _ in
      self?.presentDeleteAlert(indexPath)
    }
    if let deleteImage = UIImage.iuDelete() {
      delete.backgroundColor = UIColor(patternImage: deleteImage)
    }
    return [delete]
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    guard indexPath.section == 0 else {
      return
    }
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    guard let viewModel = homeViewModel?.students[indexPath.row].currentViewModel() as? HomeViewCellViewModel else {
      return
    }
    fetchStudent(viewModel.student)
  }
}

// MARK: ScrollView Delegate
extension HomeView: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    let factor = min(1, max(minimumLogoutLimit, (scrollView.contentOffset.y + scrollView.bounds.height - scrollView.contentSize.height) / logoutHeight))
    logoutLabel.textColor = UIColor.iuAlmostWhiteColor().colorWithAlphaComponent(factor)
    if factor < 1 {
      logoutLabel.backgroundColor = logoutLabel.backgroundColor?.colorWithAlphaComponent(logoutBackgroundOffAlpha)
    } else {
      logoutLabel.backgroundColor = logoutLabel.backgroundColor?.colorWithAlphaComponent(1)
    }
  }
  
  func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    let shouldLogout = scrollView.contentOffset.y + scrollView.bounds.height > scrollView.contentSize.height + logoutHeight
    if shouldLogout {
      delegate?.logoutPressedOnHomeView(self)
    }
  }
}
