//
//  TourPage4View.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TourPage4View: UIView, TourPageViewProtocol {
  private let classesLabel = UILabel()
  private let afterLabel = UILabel()
  private let counterLabel = UILabel()
  private let lessImageView = UIImageView()
  private let moreImageView = UIImageView()
  private let shadowImageView = UIImageView()
  private let beforeLabel = UILabel()
  private var counter = 7 {
    didSet {
      counterLabel.text = String(counter)
    }
  }
  private let logic = [-1, 1, 1, -1, 1, -1]
  private var currentLogic = -1
  
  var labels: [UIView] = []
  var images: [UIView] = []
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.iuDarkGreyColor()
    
    classesLabel.translatesAutoresizingMaskIntoConstraints = false
    classesLabel.font = UIFont.iuBlackFont(36)
    classesLabel.text = Strings.Tour4ClassGivenLabelText
    classesLabel.textColor = UIColor.whiteColor()
    classesLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(classesLabel)
    
    afterLabel.translatesAutoresizingMaskIntoConstraints = false
    afterLabel.font = UIFont.iuBlackFont(14)
    afterLabel.text = Strings.Tour4AfterLabelText
    afterLabel.textColor = UIColor.whiteColor()
    afterLabel.textAlignment = .Center
    afterLabel.numberOfLines = 2
    afterLabel.adjustsFontSizeToFitWidth = true
    afterLabel.minimumScaleFactor = 0.7
    afterLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(afterLabel)
    
    lessImageView.translatesAutoresizingMaskIntoConstraints = false
    lessImageView.image = UIImage.iuTourLess()
    lessImageView.contentMode = .Center
    addSubview(lessImageView)
    
    counterLabel.translatesAutoresizingMaskIntoConstraints = false
    counterLabel.font = UIFont.iuBlackFont(210)
    counterLabel.text = String(counter)
    counterLabel.textColor = UIColor.iuSeafoamBlueColor()
    counterLabel.adjustsFontSizeToFitWidth = true
    counterLabel.minimumScaleFactor = 0.5
    counterLabel.setContentCompressionResistancePriority(UILayoutPriorityDefaultHigh, forAxis: .Vertical)
    addSubview(counterLabel)
    
    moreImageView.translatesAutoresizingMaskIntoConstraints = false
    moreImageView.image = UIImage.iuTourMore()
    moreImageView.contentMode = .Center
    addSubview(moreImageView)
    
    shadowImageView.translatesAutoresizingMaskIntoConstraints = false
    shadowImageView.image = UIImage.iuShadow()
    shadowImageView.contentMode = .ScaleAspectFit
    shadowImageView.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    shadowImageView.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(shadowImageView)
    
    beforeLabel.translatesAutoresizingMaskIntoConstraints = false
    beforeLabel.font = UIFont.iuBlackFont(14)
    beforeLabel.text = Strings.Tour4BeforeLabelText
    beforeLabel.textColor = UIColor.whiteColor()
    beforeLabel.textAlignment = .Center
    beforeLabel.numberOfLines = 0
    beforeLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    beforeLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(beforeLabel)
    
    labels = [classesLabel, afterLabel, beforeLabel]
    images = [lessImageView, counterLabel, moreImageView, shadowImageView]
    
    let metrics = ["margin": 20]
    let views = ["classesLabel": classesLabel, "afterLabel": afterLabel, "lessImageView": lessImageView, "counterLabel": counterLabel, "moreImageView": moreImageView, "shadowImageView": shadowImageView, "beforeLabel": beforeLabel]
    [classesLabel, afterLabel, counterLabel, shadowImageView, beforeLabel].forEach {
      let horizontalConstraints = [
        NSLayoutConstraint(item: $0, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: $0, attribute: .Left, relatedBy: .GreaterThanOrEqual, toItem: self, attribute: .LeftMargin, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: $0, attribute: .Right, relatedBy: .LessThanOrEqual, toItem: self, attribute: .RightMargin, multiplier: 1, constant: 0)
      ]
      addConstraints(horizontalConstraints)
    }
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-[classesLabel]-[afterLabel][counterLabel(200)][shadowImageView]-[beforeLabel]-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    let centerConstraint = NSLayoutConstraint(item: counterLabel, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: Constants.tourViewButtonHeight / 2)
    addConstraint(centerConstraint)
    
    let lessConstraints = [
      NSLayoutConstraint(item: lessImageView, attribute: .Left, relatedBy: .Equal, toItem: self, attribute: .Left, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: lessImageView, attribute: .Right, relatedBy: .Equal, toItem: counterLabel, attribute: .Left, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: lessImageView, attribute: .Top, relatedBy: .Equal, toItem: counterLabel, attribute: .Top, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: lessImageView, attribute: .Bottom, relatedBy: .Equal, toItem: counterLabel, attribute: .Bottom, multiplier: 1, constant: 0)
    ]
    addConstraints(lessConstraints)
    let moreConstraints = [
      NSLayoutConstraint(item: moreImageView, attribute: .Left, relatedBy: .Equal, toItem: counterLabel, attribute: .Right, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: moreImageView, attribute: .Right, relatedBy: .Equal, toItem: self, attribute: .Right, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: moreImageView, attribute: .Top, relatedBy: .Equal, toItem: counterLabel, attribute: .Top, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: moreImageView, attribute: .Bottom, relatedBy: .Equal, toItem: counterLabel, attribute: .Bottom, multiplier: 1, constant: 0)
    ]
    addConstraints(moreConstraints)
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    fadeIn { [weak self] in
      self?.next()
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Private
private extension TourPage4View {
  func add() {
    vibrate(moreImageView)
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_MSEC * 100)), dispatch_get_main_queue()) { [weak self] in
      self?.counter += 1
      self?.next()
    }
  }
  
  func subtract() {
    vibrate(lessImageView)
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_MSEC * 100)), dispatch_get_main_queue()) { [weak self] in
      self?.counter -= 1
      self?.next()
    }
  }
  
  func vibrate(view: UIView) {
    let animation = CAKeyframeAnimation(keyPath: "transform.scale")
    animation.duration = 0.5
    animation.values = [0.78, 1.11, 0.94, 1.03, 0.98, 1.01, 0.995, 1]
    view.layer.addAnimation(animation, forKey: "shake")
  }
  
  func next() {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC * 1)), dispatch_get_main_queue()) { [weak self] in
      guard let weakSelf = self else {
        return
      }
      if weakSelf.currentLogic + 1 < weakSelf.logic.count {
        weakSelf.currentLogic += 1
      } else {
        weakSelf.currentLogic = 0
      }
      let logic = weakSelf.logic[weakSelf.currentLogic]
      if abs(logic) == logic {
        weakSelf.add()
      } else {
        weakSelf.subtract()
      }
    }
  }
}
