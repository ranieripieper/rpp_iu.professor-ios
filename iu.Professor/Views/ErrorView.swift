//
//  ErrorView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/25/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

struct AlertButton {
  let title: String
  let action: (() -> ())?
  
  func buttonView(highlight: Bool, alertView: AlertView) -> UIButton {
    let button = UIButton(type: .Custom)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.setTitle(title, forState: .Normal)
    button.setTitleColor(highlight ? UIColor.iuSeafoamBlueColor() : UIColor.iuBlueyGreyColor(), forState: .Normal)
    button.titleLabel?.font = UIFont.iuBlackFont(14)
    button.addTarget(alertView, action: #selector(AlertView.buttonTapped), forControlEvents: .TouchUpInside)
    
    return button
  }
}

// MARK: Equatable
extension AlertButton: Equatable {}

func ==(lhs: AlertButton, rhs: AlertButton) -> Bool {
  return lhs.title == rhs.title
}

final class AlertView: UIView {
  private let margin: CGFloat = 24
  private let textFieldPadding: CGFloat = 12
  private let hiddenAlpha: CGFloat = 0.5
  
  private let message: String
  private let buttons: [AlertButton]?
  
  override init(frame: CGRect) {
    message = ""
    buttons = []
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    message = ""
    buttons = []
    super.init(coder: aDecoder)
    setUp()
  }
  
  init(message: String, buttons: [AlertButton]?) {
    self.message = message
    self.buttons = buttons
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.iuDarkGreyColor()
    
    let messageLabel = UILabel()
    messageLabel.translatesAutoresizingMaskIntoConstraints = false
    messageLabel.text = message
    messageLabel.textColor = UIColor.iuBlueyGreyColor()
    messageLabel.textAlignment = .Right
    messageLabel.font = UIFont.iuBlackFont(14)
    messageLabel.numberOfLines = 0
    messageLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(messageLabel)
    
    let margin: CGFloat = 8
    let metrics = ["margin": margin, "verticalMargin": 30]
    var views: [String: UIView] = ["messageLabel": messageLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|->=margin-[messageLabel]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    if let buttons = buttons where buttons.count > 0, let first = buttons.first {
      let firstButton = first.buttonView(true, alertView: self)
      firstButton.tag = 1
      addSubview(firstButton)
      views["firstButton"] = firstButton
      horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[firstButton]-margin-|", options: [], metrics: metrics, views: views)
      addConstraints(horizontalConstraints)
      for i in 1..<buttons.count {
        let previousButton = viewWithTag(i)
        let button = buttons[i].buttonView(false, alertView: self)
        button.tag = i + 1
        addSubview(button)
        let constraints = [
          NSLayoutConstraint(item: button, attribute: .Right, relatedBy: .Equal, toItem: previousButton, attribute: .Left, multiplier: 1, constant: -margin * 2),
          NSLayoutConstraint(item: button, attribute: .CenterY, relatedBy: .Equal, toItem: previousButton, attribute: .CenterY, multiplier: 1, constant: 0)
        ]
        addConstraints(constraints)
      }
      let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-verticalMargin-[messageLabel]-verticalMargin-[firstButton]-|", options: [], metrics: metrics, views: views)
      addConstraints(verticalConstraints)
    } else {
      let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-verticalMargin-[messageLabel]-verticalMargin-|", options: [], metrics: metrics, views: views)
      addConstraints(verticalConstraints)
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Public
extension AlertView {
  static func presentError(message: String, buttons: [AlertButton]?, inView view: UIView?) {
    guard let view = view else {
      return
    }
    dispatch_async(dispatch_get_main_queue()) {
      let errorView = AlertView(message: message, buttons: buttons)
      view.addSubview(errorView)
      
      let views = ["errorView": errorView]
      let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[errorView]|", options: [], metrics: nil, views: views)
      view.addConstraints(horizontalConstraints)
      let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[errorView]", options: [], metrics: nil, views: views)
      view.addConstraints(verticalConstraints)
      view.layoutIfNeeded()
      errorView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -errorView.bounds.height)
      UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: Constants.defaultAnimationDelay, usingSpringWithDamping: Constants.defaultAnimationDamping, initialSpringVelocity: Constants.defaultAnimationInitialVelocity, options: .CurveEaseInOut, animations: {
        errorView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -0.1 * errorView.bounds.height)
        }, completion: { _ in
          if buttons == nil {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * Int64(NSEC_PER_SEC)), dispatch_get_main_queue()) {
              errorView.dismiss()
            }
          }
      })
    }
  }
  
  static func presentVersionAlert(inView view: UIView, topPadding: CGFloat, belowSubview: UIView, mandatory: Bool) {
    let buttons: [AlertButton]
    let updateButtonAction: () -> () = {
      guard let url = NSURL(string: Constants.appStoreLink) else {
        return
      }
      UIApplication.sharedApplication().openURL(url)
    }
    if mandatory {
      buttons = [
        AlertButton(title: Strings.AlertUpdateNowButton, action: updateButtonAction)
      ]
    } else {
      buttons = [
        AlertButton(title: Strings.AlertUpdateButton, action: updateButtonAction),
        AlertButton(title: Strings.AlertCancelButton, action: nil)
      ]
    }
    dispatch_async(dispatch_get_main_queue()) {
      let errorView = AlertView(message: Strings.AlertVersionLabel, buttons: buttons)
      view.insertSubview(errorView, belowSubview: belowSubview)
      
      let metrics = ["topPadding": topPadding]
      let views = ["errorView": errorView]
      let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[errorView]|", options: [], metrics: nil, views: views)
      view.addConstraints(horizontalConstraints)
      let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-topPadding-[errorView]", options: [], metrics: metrics, views: views)
      view.addConstraints(verticalConstraints)
      view.layoutIfNeeded()
      errorView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -errorView.bounds.height)
      UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: Constants.defaultAnimationDelay, usingSpringWithDamping: Constants.defaultAnimationDamping, initialSpringVelocity: Constants.defaultAnimationInitialVelocity, options: .CurveEaseInOut, animations: {
        errorView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -0.1 * errorView.bounds.height)
        }, completion: nil)
    }
  }
}

// MARK: Private
private extension AlertView {
  func dismiss() {
    dispatch_async(dispatch_get_main_queue()) {
      UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: Constants.defaultAnimationDelay, usingSpringWithDamping: Constants.defaultAnimationDamping, initialSpringVelocity: Constants.defaultAnimationInitialVelocity, options: .CurveEaseInOut, animations: { [weak self] in
        if let weakSelf = self {
          weakSelf.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -weakSelf.bounds.height - weakSelf.frame.origin.y)
          weakSelf.alpha = 0
        }
        }, completion: { [weak self] _ in
          self?.removeFromSuperview()
        })
    }
  }
}

// MARK: Actions
extension AlertView {
  func buttonTapped(button: UIButton) {
    let tag = button.tag - 1
    buttons?[tag].action?()
    dismiss()
  }
}
