//
//  HomeViewController.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class HomeViewController: UIViewController {
  private let homeView = HomeView()
  
  weak var navigationDelegate: NavigationControllerDelegate?
  
  override func loadView() {
    view = homeView
    homeView.delegate = self
    AnswersHelper.pageView(Constants.answersPageViewHome)
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    HomeViewModel.viewModel { [weak self] homeViewModel in
      self?.homeView.homeViewModel = homeViewModel
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Home View Delegate
extension HomeViewController: HomeViewDelegate {
  func addAddButtonToHomeViewModel(homeViewModel: HomeViewModel) {
    navigationItem.leftBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .Add, target: homeView, action: #selector(HomeView.startButtonPressed))]
  }
  
  func removeAddButtonToHomeViewModel(homeViewModel: HomeViewModel) {
    navigationItem.leftBarButtonItems = []
  }
  
  func addStudent(existingPhones: [Double]) {
    let newStudentViewController = NewStudentViewController(existingPhones: existingPhones ?? [])
    navigationController?.pushViewController(newStudentViewController, animated: true)
  }

  func goToStudent(studentViewModel: StudentViewModel) {
    let studentViewController = StudentViewController(studentViewModel: studentViewModel)
    navigationController?.pushViewController(studentViewController, animated: true)
  }
  
  func goToPaymentType(student: Student) {
    let paymentTypeViewController = PaymentTypeViewController(student: student, newStudent: false)
    navigationController?.pushViewController(paymentTypeViewController, animated: true)
  }
  
  func logoutPressedOnHomeView(homeView: HomeView) {
    ParseHelper.signout()
    navigationDelegate?.goToLogin()
  }
}

extension HomeViewController: NavigationControllerProtocol {}
