//
//  SignupView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol SignupViewDelegate: class {
  func signupViewDidRegister(signupView: SignupView?)
}

final class SignupView: UIView {
  private let margin: CGFloat = 24
  private let textFieldPadding: CGFloat = 12
  private let hiddenAlpha: CGFloat = 0.5
  
  let scrollView = UIScrollView()
  private let nameTextField = UITextField()
  private let emailTextField = UITextField()
  private let passwordTextField = UITextField()
  private let passwordConfirmationTextField = UITextField()
  private let signupButton = UIButton(type: .Custom)
  
  private let signupViewModel = SignupViewModel()
  
  weak var delegate: SignupViewDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    
    let scrollViewContentView = UIView()
    scrollViewContentView.translatesAutoresizingMaskIntoConstraints = false
    
    nameTextField.translatesAutoresizingMaskIntoConstraints = false
    nameTextField.placeholder = Strings.SignupTypeName
    nameTextField.font = UIFont.iuHeavyFont(16)
    nameTextField.textColor = UIColor.iuLightGreyColor()
    nameTextField.tintColor = UIColor.iuLightBlueGreyColor()
    nameTextField.backgroundColor = UIColor.whiteColor()
    nameTextField.layer.cornerRadius = 8
    nameTextField.autocapitalizationType = .Words
    nameTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    nameTextField.leftViewMode = .Always
    nameTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    nameTextField.rightViewMode = .Always
    nameTextField.delegate = self
    
    emailTextField.translatesAutoresizingMaskIntoConstraints = false
    emailTextField.placeholder = Strings.SignupTypeEmail
    emailTextField.font = UIFont.iuHeavyFont(16)
    emailTextField.textColor = UIColor.iuLightGreyColor()
    emailTextField.tintColor = UIColor.iuLightBlueGreyColor()
    emailTextField.backgroundColor = UIColor.whiteColor()
    emailTextField.keyboardType = .EmailAddress
    emailTextField.autocorrectionType = .No
    emailTextField.layer.cornerRadius = 8
    emailTextField.autocapitalizationType = .None
    emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    emailTextField.leftViewMode = .Always
    emailTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    emailTextField.rightViewMode = .Always
    emailTextField.delegate = self
    
    passwordTextField.translatesAutoresizingMaskIntoConstraints = false
    passwordTextField.placeholder = Strings.SignupTypePassword
    passwordTextField.font = UIFont.iuHeavyFont(16)
    passwordTextField.textColor = UIColor.iuLightGreyColor()
    passwordTextField.tintColor = UIColor.iuLightBlueGreyColor()
    passwordTextField.backgroundColor = UIColor.whiteColor()
    passwordTextField.secureTextEntry = true
    passwordTextField.layer.cornerRadius = 8
    passwordTextField.autocapitalizationType = .None
    passwordTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    passwordTextField.leftViewMode = .Always
    passwordTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    passwordTextField.rightViewMode = .Always
    passwordTextField.delegate = self
    
    passwordConfirmationTextField.translatesAutoresizingMaskIntoConstraints = false
    passwordConfirmationTextField.placeholder = Strings.SignupTypePasswordConfirmation
    passwordConfirmationTextField.font = UIFont.iuHeavyFont(16)
    passwordConfirmationTextField.textColor = UIColor.iuLightGreyColor()
    passwordConfirmationTextField.tintColor = UIColor.iuLightBlueGreyColor()
    passwordConfirmationTextField.backgroundColor = UIColor.whiteColor()
    passwordConfirmationTextField.secureTextEntry = true
    passwordConfirmationTextField.layer.cornerRadius = 8
    passwordConfirmationTextField.autocapitalizationType = .None
    passwordConfirmationTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    passwordConfirmationTextField.leftViewMode = .Always
    passwordConfirmationTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    passwordConfirmationTextField.rightViewMode = .Always
    passwordConfirmationTextField.delegate = self
    
    signupButton.translatesAutoresizingMaskIntoConstraints = false
    signupButton.backgroundColor = UIColor.iuUglyPinkColor()
    signupButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    signupButton.titleLabel?.font = UIFont.iuBlackFont(18)
    signupButton.setTitle(Strings.SignupButton, forState: .Normal)
    signupButton.addTarget(self, action: #selector(SignupView.signupButtonPressed), forControlEvents: .TouchUpInside)
    signupButton.layer.cornerRadius = 8
    signupButton.enabled = false
    signupButton.alpha = hiddenAlpha
    
    let container = UIView()
    container.translatesAutoresizingMaskIntoConstraints = false
    
    container.addSubview(nameTextField)
    container.addSubview(emailTextField)
    container.addSubview(passwordTextField)
    container.addSubview(passwordConfirmationTextField)
    container.addSubview(signupButton)
    scrollViewContentView.addSubview(container)
    scrollView.addSubview(scrollViewContentView)
    addSubview(scrollView)
    
    let views = ["nameTextField": nameTextField, "emailTextField": emailTextField, "passwordTextField": passwordTextField, "passwordConfirmationTextField": passwordConfirmationTextField, "signupButton": signupButton, "container": container, "scrollViewContentView": scrollViewContentView, "scrollView": scrollView]
    let metrics = ["margin": margin, "margin2": margin * 1.2, "height": 64]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[nameTextField]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[emailTextField]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[passwordTextField]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[passwordConfirmationTextField]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[signupButton]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[nameTextField(height)]-margin-[emailTextField(height)]-margin-[passwordTextField(height)]-margin-[passwordConfirmationTextField(height)]-margin2-[signupButton(height)]|", options: [], metrics: metrics, views: views)
    container.addConstraints(verticalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: container, attribute: .CenterY, relatedBy: .Equal, toItem: scrollViewContentView, attribute: .CenterY, multiplier: 1, constant: 0)]
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[container]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|->=0-[container]->=0-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollViewContentView]|", options: [], metrics: metrics, views: views)
    scrollView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollViewContentView]|", options: [], metrics: metrics, views: views)
    scrollView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollView]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: scrollViewContentView, attribute: .Width, relatedBy: .Equal, toItem: self, attribute: .Width, multiplier: 1, constant: 0)]
    addConstraints(horizontalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: scrollViewContentView, attribute: .Height, relatedBy: .Equal, toItem: self, attribute: .Height, multiplier: 1, constant: 0)]
    addConstraints(verticalConstraints)
    
    nameTextField.becomeFirstResponder()
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Private
private extension SignupView {
  func startLoading() -> LoadingView {
    signupButton.userInteractionEnabled = false
    
    let loadingView = LoadingView()
    loadingView.frame = signupButton.frame
    loadingView.alpha = 0
    signupButton.superview?.addSubview(loadingView)
    UIView.animateWithDuration(Constants.defaultAnimationDuration) { [weak self] in
      loadingView.alpha = 1
      self?.signupButton.alpha = 0
    }
    return loadingView
  }
  
  func endLoading(loadingView: LoadingView) {
    loadingView.endAnimating()
    UIView.animateWithDuration(Constants.defaultAnimationDuration, animations: { [weak self] in
      loadingView.alpha = 0
      self?.signupButton.alpha = 1
      }, completion: { [weak self] _ in
        self?.signupButton.userInteractionEnabled = true
        loadingView.removeFromSuperview()
      })
  }
}

// MARK: Actions
extension SignupView {
  func signupButtonPressed() {
    endEditing(true)
    let loadingView = startLoading()
    signupViewModel.signup(nameTextField.text, email: emailTextField.text, password: passwordTextField.text, passwordConfirmation: passwordConfirmationTextField.text) { [weak self] inner in
      self?.endLoading(loadingView)
      do {
        try inner()
        self?.delegate?.signupViewDidRegister(self)
      } catch let error as SignupError {
        AlertView.presentError(error.description(), buttons: nil, inView: self)
      } catch {
        AlertView.presentError(SignupError.ExistingEmail.description(), buttons: nil, inView: self)
      }
    }
  }
}

// MARK: Text Field Delegate
extension SignupView: UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    switch textField {
    case nameTextField:
      emailTextField.becomeFirstResponder()
    case emailTextField:
      passwordTextField.becomeFirstResponder()
    case passwordTextField:
      passwordConfirmationTextField.becomeFirstResponder()
    case passwordConfirmationTextField:
      passwordConfirmationTextField.resignFirstResponder()
    default:
      break
    }
    return true
  }
  
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    let startIndex = textField.text!.startIndex.advancedBy(range.location)
    let endIndex = startIndex.advancedBy(range.length)
    let indexRange = startIndex..<endIndex
    let newString = textField.text!.stringByReplacingCharactersInRange(indexRange, withString: string)
    let allFieldsFilled: Bool = {
      switch textField {
      case self.nameTextField:
        return newString.characters.count > 0 && emailTextField.text?.characters.count > 0 && passwordTextField.text?.characters.count > 0 && passwordConfirmationTextField.text?.characters.count > 0
      case self.emailTextField:
        return nameTextField.text?.characters.count > 0 && newString.characters.count > 0 && passwordTextField.text?.characters.count > 0 && passwordConfirmationTextField.text?.characters.count > 0
      case self.passwordTextField:
        return nameTextField.text?.characters.count > 0 && emailTextField.text?.characters.count > 0 && newString.characters.count > 0 && passwordConfirmationTextField.text?.characters.count > 0
      case self.passwordConfirmationTextField:
        return nameTextField.text?.characters.count > 0 && emailTextField.text?.characters.count > 0 && passwordTextField.text?.characters.count > 0 && newString.characters.count > 0
      default:
        return nameTextField.text?.characters.count > 0 && emailTextField.text?.characters.count > 0 && passwordTextField.text?.characters.count > 0 && passwordConfirmationTextField.text?.characters.count > 0
      }
    }()
    signupButton.enabled = allFieldsFilled
    signupButton.alpha = allFieldsFilled ? 1 : hiddenAlpha
    return true
  }
}

extension UIScrollView {
  override public func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    if dragging {
      super.touchesEnded(touches, withEvent: event)
    } else {
      superview?.endEditing(true)
    }
  }
}
