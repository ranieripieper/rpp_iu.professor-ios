//
//  NewStudentViewController.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/23/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class NewStudentViewController: UIViewController {
  private let existingPhones: [Double]
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(existingPhones: [Double]) {
    self.existingPhones = existingPhones
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    let newStudentView = NewStudentView()
    newStudentView.delegate = self
    view = newStudentView
    AnswersHelper.pageView(Constants.answersPageViewNewStudent)
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: New Student View Delegate
extension NewStudentViewController: NewStudentViewDelegate {
  func newStudentView(newStudentView: NewStudentView, tappedNextWithName name: String, phone: Double) {
    guard !existingPhones.contains(phone) else {
      AlertView.presentError(Strings.NewStudentExistingNumberErrorAlert, buttons: nil, inView: view)
      return
    }
    let student = Student()
    student["name"] = name
    student["phone"] = phone
    let paymentTypeViewController = PaymentTypeViewController(student: student, newStudent: true)
    navigationController?.pushViewController(paymentTypeViewController, animated: true)
  }
}
