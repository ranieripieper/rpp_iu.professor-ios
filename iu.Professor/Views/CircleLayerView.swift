//
//  CircleLayerView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class CircleLayerView: UIView {
  private let numberOfCircles: Int
  
  required init?(coder aDecoder: NSCoder) {
    numberOfCircles = 1
    super.init(coder: aDecoder)
    setUp()
  }
  
  init(circles: Int, width: CGFloat) {
    numberOfCircles = circles
    super.init(frame: CGRect(x: 0, y: 0, width: width, height: width))
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.clearColor()
    
    for i in 0..<numberOfCircles {
      let radius = bounds.width / CGFloat(2 + i)
      let circleLayer = self.layer(radius)
      layer.addSublayer(circleLayer)
    }
    wanderAround()
  }
}

// MARK: Private
private extension CircleLayerView {
  func layer(radius: CGFloat) -> CAShapeLayer {
    let width = bounds.width
    let circleLayer = CAShapeLayer()
    circleLayer.path = UIBezierPath(roundedRect: CGRect(x: width / 2 - radius, y: width / 2 - radius, width: 2.0 * radius, height: 2.0 * radius), cornerRadius: radius).CGPath
    circleLayer.strokeColor = UIColor.whiteColor().CGColor
    circleLayer.fillColor = UIColor.clearColor().CGColor
    return circleLayer
  }
  
  func wanderAround() {
    let xAnimation = CAKeyframeAnimation(keyPath: "transform.translation.x")
    xAnimation.duration = 10
    var xValues = [Int]()
    for _ in 0...5 {
      xValues.append(random())
    }
    if let xFirstValue = xValues.first {
      xValues.append(xFirstValue)
    }
    xAnimation.values = xValues
    xAnimation.additive = true
    xAnimation.repeatCount = Float.infinity
    layer.addAnimation(xAnimation, forKey: "x")
    
    let yAnimation = CAKeyframeAnimation(keyPath: "transform.translation.y")
    yAnimation.duration = 10
    var yValues = [Int]()
    for _ in 0...5 {
      yValues.append(random())
    }
    if let yFirstValue = yValues.first {
      yValues.append(yFirstValue)
    }
    yAnimation.values = yValues
    yAnimation.additive = true
    yAnimation.repeatCount = Float.infinity
    layer.addAnimation(yAnimation, forKey: "y")
  }
  
  func random() -> Int {
    let random = arc4random() % 9
    let sign = arc4random() % 2 == 0 ? 1 : -1
    return sign * Int(random)
  }
}

extension CircleLayerView {
  func pop() {
    transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001)
    alpha = 1
    UIView.animateWithDuration(0.2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: { [weak self] in
      self?.transform = CGAffineTransformIdentity
    }, completion: nil)
  }
}
