//
//  WiggleAnimation.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol WiggleProtocol: class {
  var layer: CALayer { get }
  
  func wiggle()
}

extension WiggleProtocol {
  func wiggle() {
    let animation = CAKeyframeAnimation(keyPath: "transform.rotation")
    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
    animation.duration = 0.4
    animation.values = [-M_PI/10, M_PI/10, -M_PI/10, M_PI/10, -M_PI/12, M_PI/12, -M_PI/20, M_PI/20, 0]
    layer.addAnimation(animation, forKey: "shake")
  }
}
