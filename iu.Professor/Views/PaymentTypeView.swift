//
//  PaymentTypeView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 4/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol PaymentTypeViewDelegate: class {
  func nextOnPaymentTypeView(studentViewModel: StudentViewModel)
}

final class PaymentTypeView: UIView {
  private let paymentTypeViewModel: PaymentTypeViewModel
  
  private let beforeButton = UIButton(type: .Custom)
  private let beforeLabel = UILabel()
  private let afterButton = UIButton(type: .Custom)
  private let afterLabel = UILabel()
  private let nextButton = UIButton(type: .Custom)
  
  weak var delegate: PaymentTypeViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(student: Student) {
    paymentTypeViewModel = PaymentTypeViewModel(student: student)
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    let topLabel = UILabel()
    topLabel.translatesAutoresizingMaskIntoConstraints = false
    topLabel.textAlignment = .Center
    topLabel.textColor = UIColor.iuLightGreyColor()
    topLabel.text = Strings.PaymentTypeTopLabel
    topLabel.font = UIFont.iuBlackFont(18)
    addSubview(topLabel)
    
    beforeButton.translatesAutoresizingMaskIntoConstraints = false
    beforeButton.setTitleColor(UIColor.iuLightBlueGreyColor(), forState: .Normal)
    beforeButton.setTitleColor(UIColor.iuSeafoamBlueColor(), forState: .Selected)
    beforeButton.setTitle(Strings.PaymentTypeBeforeButton, forState: .Normal)
    beforeButton.titleLabel?.font = UIFont.iuBlackFont(48)
    beforeButton.addTarget(self, action: #selector(PaymentTypeView.beforeButtonTapped), forControlEvents: .TouchUpInside)
    addSubview(beforeButton)
    
    beforeLabel.translatesAutoresizingMaskIntoConstraints = false
    beforeLabel.textAlignment = .Center
    beforeLabel.textColor = UIColor.iuLightGreyColor()
    beforeLabel.text = Strings.PaymentTypeBeforeLabel.stringByReplacingOccurrencesOfString("%@", withString: paymentTypeViewModel.studentName)
    beforeLabel.font = UIFont.iuBlackFont(14)
    beforeLabel.numberOfLines = 0
    addSubview(beforeLabel)
    
    afterButton.translatesAutoresizingMaskIntoConstraints = false
    afterButton.setTitleColor(UIColor.iuLightBlueGreyColor(), forState: .Normal)
    afterButton.setTitleColor(UIColor.iuSeafoamBlueColor(), forState: .Selected)
    afterButton.setTitle(Strings.PaymentTypeAfterButton, forState: .Normal)
    afterButton.titleLabel?.font = UIFont.iuBlackFont(48)
    afterButton.addTarget(self, action: #selector(PaymentTypeView.afterButtonTapped), forControlEvents: .TouchUpInside)
    addSubview(afterButton)
    
    afterLabel.translatesAutoresizingMaskIntoConstraints = false
    afterLabel.textAlignment = .Center
    afterLabel.textColor = UIColor.iuLightGreyColor()
    afterLabel.text = Strings.PaymentTypeAfterLabel.stringByReplacingOccurrencesOfString("%@", withString: paymentTypeViewModel.studentName)
    afterLabel.font = UIFont.iuBlackFont(14)
    afterLabel.numberOfLines = 0
    addSubview(afterLabel)
    
    nextButton.translatesAutoresizingMaskIntoConstraints = false
    nextButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    nextButton.setTitle(Strings.PaymentTypeNextButton, forState: .Normal)
    nextButton.titleLabel?.font = UIFont.iuBlackFont(18)
    nextButton.backgroundColor = UIColor.iuUglyPinkColor()
    nextButton.layer.cornerRadius = 8
    nextButton.addTarget(self, action: #selector(PaymentTypeView.nextButtonTapped), forControlEvents: .TouchUpInside)
    addSubview(nextButton)
    nextButton.hidden = true
    
    let metrics = ["margin": 30, "buttonHeight": 64]
    let views = ["topLabel": topLabel, "beforeButton": beforeButton, "beforeLabel": beforeLabel, "afterButton": afterButton, "afterLabel": afterLabel, "nextButton": nextButton]
    
    [topLabel, beforeButton, beforeLabel, afterButton, afterLabel, nextButton].forEach {
      let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[view]-margin-|", options: [], metrics: metrics, views: ["view": $0])
      addConstraints(horizontalConstraints)
    }
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-50-[topLabel]-margin-[beforeButton][beforeLabel]-margin-[afterButton][afterLabel]-margin-[nextButton(buttonHeight)]", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Actions
extension PaymentTypeView {
  func beforeButtonTapped() {
    beforeButton.selected = true
    beforeLabel.textColor = UIColor.iuGreyishColor()
    afterButton.selected = false
    afterLabel.textColor = UIColor.iuLightGreyColor()
    
    nextButton.hidden = false
  }
  
  func afterButtonTapped() {
    beforeButton.selected = false
    beforeLabel.textColor = UIColor.iuLightGreyColor()
    afterButton.selected = true
    afterLabel.textColor = UIColor.iuGreyishColor()
    
    nextButton.hidden = false
  }
  
  func nextButtonTapped() {
    let studentViewModel = paymentTypeViewModel.studentViewModel(beforeButton.selected)
    delegate?.nextOnPaymentTypeView(studentViewModel)
  }
}
