//
//  TourModelTransitioningDelegate.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TourModelTransitioningDelegate: NSObject, UIViewControllerAnimatedTransitioning {
  private let firstTransitionDuration = NSTimeInterval(1 - Constants.navigationBarHeight / UIScreen.mainScreen().bounds.height * Constants.tourDismissalTransitionDuration)
  private let secondTransitionDuration = NSTimeInterval(Constants.navigationBarHeight / UIScreen.mainScreen().bounds.height * Constants.tourDismissalTransitionDuration)
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
    return firstTransitionDuration
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    guard let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) else {
      return
    }
    UIView.animateWithDuration(transitionDuration(transitionContext), delay: Constants.defaultAnimationDelay, options: .CurveEaseIn, animations: {
      fromViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, Constants.tourViewButtonHeight + Constants.navigationBarHeight - fromViewController.view.bounds.height)
    }) { [weak self] finished in
      if let tourView = fromViewController.view as? TourView {
        UIView.animateWithDuration(self?.secondTransitionDuration ?? Constants.defaultAnimationDuration, delay: Constants.defaultAnimationDelay, options: .CurveEaseOut, animations: {
          tourView.button.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -Constants.tourViewButtonHeight)
          }, completion: { aFinished in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        })
      } else {
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension TourModelTransitioningDelegate: UIViewControllerTransitioningDelegate {
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return self
  }
}
