//
//  UseClassesViewController.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class UseClassesViewController: UIViewController {
  let useClassesView = UseClassesView()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init(studentViewModel: StudentViewModel, expireDate: NSDate, classesCount: Int, useClassesCount: Int) {
    super.init(nibName: nil, bundle: nil)
    useClassesView.useClassesViewModel = UseClassesViewModel(studentViewModel: studentViewModel, expireDate: expireDate, classesCount: classesCount, useClassesCount: useClassesCount)
    useClassesView.delegate = self
  }
  
  override func loadView() {
    view = useClassesView
    AnswersHelper.pageView(Constants.answersPageViewCalendar)
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Use Classes View Delegate
extension UseClassesViewController: UseClassesViewDelegate {
  func didSave() {
    navigationController?.popToRootViewControllerAnimated(true)
  }
  
  func invite(name: String) {
    let inviteViewController = InviteViewController(name: name)
    navigationController?.pushViewController(inviteViewController, animated: true)
  }
}
