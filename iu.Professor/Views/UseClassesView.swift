//
//  UseClassesView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import CVCalendar

protocol UseClassesViewDelegate: class {
  func didSave()
  func invite(name: String)
}

final class UseClassesView: UIView {
  private let scrollView = UIScrollView()
  private let titleLabel = UILabel()
  private let monthLabel = UILabel()
  private let calendarMenuView = CVCalendarMenuView(frame: .zero)
  private let calendarView = CVCalendarView(frame: .zero)
  private let okButton = UIButton(type: .Custom)
  private var currentDayView: DayView?
  private var selectedDayViews: [UIView] = []
  
  private var selectedDate = NSDate()
  private let dateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    dateFormatter.dateFormat = "MMMM yyyy"
    return dateFormatter
  }()
  private lazy var animatableCircle: UIView = {
    let animatableCircle = UIView()
    animatableCircle.backgroundColor = UIColor.iuSeafoamBlueColor()
    animatableCircle.clipsToBounds = true
    return animatableCircle
  }()
  private lazy var selectedDatesView: UIView = {
    let selectedDatesView = UIView()
    selectedDatesView.translatesAutoresizingMaskIntoConstraints = false
    selectedDatesView.backgroundColor = UIColor.iuLightBlueGreyColor()
    self.addSubview(selectedDatesView)
    
    let views = ["selectedDatesView": selectedDatesView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[selectedDatesView]|", options: [], metrics: nil, views: views)
    self.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[selectedDatesView]|", options: [], metrics: nil, views: views)
    self.addConstraints(verticalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: selectedDatesView, attribute: .Height, relatedBy: .LessThanOrEqual, toItem: self, attribute: .Height, multiplier: 0.5, constant: 0)
    ]
    self.addConstraints(verticalConstraints)
    
    return selectedDatesView
  }()
  private var classesGivenCollectionView: UICollectionView?
  private var classesGivenViewModel: ClassesGivenViewModel?
  private var collectionViewHeightConstraint: NSLayoutConstraint?
  
  var useClassesViewModel: UseClassesViewModel! {
    didSet {
      if useClassesViewModel.selectedDates.count >= useClassesViewModel.useClassesCount - 1 {
        changeOkToSave()
      } else {
        changeSaveToOk()
      }
    }
  }
  weak var delegate: UseClassesViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    useClassesViewModel = nil
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    useClassesViewModel = nil
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(scrollView)
    
    let scrollViewContentView = UIView()
    scrollViewContentView.translatesAutoresizingMaskIntoConstraints = false
    scrollView.addSubview(scrollViewContentView)
    
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.text = Strings.UseClassesTitle
    titleLabel.textColor = UIColor.iuLightGreyColor()
    titleLabel.textAlignment = .Center
    titleLabel.font = UIFont.iuBlackFont(18)
    scrollViewContentView.addSubview(titleLabel)
    
    monthLabel.translatesAutoresizingMaskIntoConstraints = false
    monthLabel.text = dateFormatter.stringFromDate(NSDate()).capitalizedString
    monthLabel.textColor = UIColor.iuLightBlueGreyTwoColor()
    monthLabel.textAlignment = .Center
    monthLabel.font = UIFont.iuBlackFont(24)
    scrollViewContentView.addSubview(monthLabel)
    
    calendarMenuView.translatesAutoresizingMaskIntoConstraints = false
    calendarMenuView.menuViewDelegate = self
    scrollViewContentView.addSubview(calendarMenuView)
    
    calendarView.translatesAutoresizingMaskIntoConstraints = false
    calendarView.calendarAppearanceDelegate = self
    calendarView.calendarDelegate = self
    calendarView.delegate = self
    calendarView.animatorDelegate = self
    scrollViewContentView.addSubview(calendarView)
    if let monthController = calendarView.contentController as? MonthContentViewController {
      let day = NSCalendar.currentCalendar().component(.Day, fromDate: NSDate())
      monthController.selectDayViewWithDay(day, inMonthView: monthController.presentedMonthView)
    }
    
    okButton.translatesAutoresizingMaskIntoConstraints = false
    okButton.backgroundColor = UIColor.iuUglyPinkColor()
    okButton.titleLabel?.font = UIFont.iuBlackFont(18)
    okButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    okButton.addTarget(self, action: #selector(UseClassesView.okButtonTapped), forControlEvents: .TouchUpInside)
    okButton.layer.cornerRadius = 8
    scrollViewContentView.addSubview(okButton)
    
    let metrics = ["margin": 20, "margin2": 30, "buttonHeight": 64]
    let views = ["scrollView": scrollView, "scrollViewContentView": scrollViewContentView, "titleLabel": titleLabel, "monthLabel": monthLabel, "calendarMenuView": calendarMenuView, "calendarView": calendarView, "okButton": okButton]
    
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollView]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollViewContentView]|", options: [], metrics: metrics, views: views)
    scrollView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollViewContentView]|", options: [], metrics: metrics, views: views)
    scrollView.addConstraints(verticalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: scrollViewContentView, attribute: .Width, relatedBy: .Equal, toItem: self, attribute: .Width, multiplier: 1, constant: 0)]
    addConstraints(horizontalConstraints)
    verticalConstraints = [NSLayoutConstraint(item: scrollViewContentView, attribute: .Height, relatedBy: .GreaterThanOrEqual, toItem: self, attribute: .Height, multiplier: 1, constant: 0)]
    addConstraints(verticalConstraints)
    
    horizontalConstraints = [
      NSLayoutConstraint(item: titleLabel, attribute: .CenterX, relatedBy: .Equal, toItem: scrollViewContentView, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: calendarView, attribute: .CenterX, relatedBy: .Equal, toItem: scrollViewContentView, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: calendarMenuView, attribute: .Left, relatedBy: .Equal, toItem: calendarView, attribute: .Left, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: calendarMenuView, attribute: .Right, relatedBy: .Equal, toItem: calendarView, attribute: .Right, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: monthLabel, attribute: .Left, relatedBy: .Equal, toItem: calendarView, attribute: .Left, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: monthLabel, attribute: .Right, relatedBy: .Equal, toItem: calendarView, attribute: .Right, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: okButton, attribute: .Left, relatedBy: .Equal, toItem: calendarView, attribute: .Left, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: okButton, attribute: .Right, relatedBy: .Equal, toItem: calendarView, attribute: .Right, multiplier: 1, constant: 0)
    ]
    scrollViewContentView.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[calendarView(300)]", options: [], metrics: nil, views: views)
    scrollViewContentView.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[titleLabel]-margin-[monthLabel]-margin-[calendarMenuView(20)]-[calendarView(250)]-margin-[okButton(buttonHeight)]-margin2-|", options: [], metrics: metrics, views: views)
    scrollViewContentView.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    if calendarView.bounds.width != 0 {
      calendarView.commitCalendarViewUpdate()
      calendarMenuView.commitMenuViewUpdate()
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Private
private extension UseClassesView {
  func changeOkToSave() {
    okButton.setTitle(Strings.UseClassesSaveButtonTitle.uppercaseString, forState: .Normal)
    okButton.removeTarget(self, action: #selector(UseClassesView.okButtonTapped), forControlEvents: .TouchUpInside)
    okButton.addTarget(self, action: #selector(UseClassesView.saveButtonTapped), forControlEvents: .TouchUpInside)
  }
  
  func changeSaveToOk() {
    okButton.setTitle(Strings.UseClassesOkButtonTitle.uppercaseString, forState: .Normal)
    okButton.removeTarget(self, action: #selector(UseClassesView.saveButtonTapped), forControlEvents: .TouchUpInside)
    okButton.addTarget(self, action: #selector(UseClassesView.okButtonTapped), forControlEvents: .TouchUpInside)
  }
  
  func startLoading() -> LoadingView {
    calendarView.userInteractionEnabled = false
    okButton.userInteractionEnabled = false
    
    let loadingView = LoadingView()
    loadingView.frame = okButton.frame
    loadingView.alpha = 0
    okButton.userInteractionEnabled = false
    okButton.superview?.addSubview(loadingView)
    UIView.animateWithDuration(Constants.defaultAnimationDuration) { [weak self] in
      loadingView.alpha = 1
      self?.okButton.alpha = 0
    }
    return loadingView
  }
  
  func endLoading(loadingView: LoadingView) {
    calendarView.userInteractionEnabled = true
    okButton.userInteractionEnabled = true
    
    loadingView.endAnimating()
    UIView.animateWithDuration(Constants.defaultAnimationDuration, animations: { [weak self] in
      loadingView.alpha = 0
      self?.okButton.alpha = 1
      }, completion: { [weak self] _ in
        self?.okButton.userInteractionEnabled = true
        loadingView.removeFromSuperview()
      })
  }
}

// MARK: Actions
extension UseClassesView {
  func okButtonTapped() {
    useClassesViewModel.selectedDates.append(selectedDate)
    
    classesGivenViewModel = ClassesGivenViewModel(classes: useClassesViewModel.selectedDatesStrings, style: .White)
    if classesGivenCollectionView == nil {
      let classesGivenCollectionView = UICollectionView(frame: .zero, collectionViewLayout: DatesFlowLayout(style: .White))
      classesGivenCollectionView.backgroundColor = UIColor.clearColor()
      classesGivenCollectionView.translatesAutoresizingMaskIntoConstraints = false
      classesGivenCollectionView.dataSource = self
      classesGivenCollectionView.delegate = self
      selectedDatesView.addSubview(classesGivenCollectionView)
      
      let views = ["collectionView": classesGivenCollectionView]
      let horizontalConstraints = [
        NSLayoutConstraint(item: classesGivenCollectionView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 280),
        NSLayoutConstraint(item: classesGivenCollectionView, attribute: .CenterX, relatedBy: .Equal, toItem: selectedDatesView, attribute: .CenterX, multiplier: 1, constant: 0)
      ]
      selectedDatesView.addConstraints(horizontalConstraints)
      let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[collectionView]|", options: [], metrics: nil, views: views)
      selectedDatesView.addConstraints(verticalConstraints)
      collectionViewHeightConstraint = NSLayoutConstraint(item: classesGivenCollectionView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 52)
      collectionViewHeightConstraint?.priority = UILayoutPriorityDefaultHigh
      classesGivenCollectionView.addConstraint(collectionViewHeightConstraint!)
      
      self.classesGivenCollectionView = classesGivenCollectionView
    }
    classesGivenViewModel?.classes.forEach {
      classesGivenCollectionView?.registerClass($0.cellClass, forCellWithReuseIdentifier: $0.reuseIdentifier)
    }
    classesGivenCollectionView?.reloadData()
    if let currentDayView = currentDayView {
      let persistedDayView = UIView()
      persistedDayView.backgroundColor = UIColor.iuLightBlueGreyColor()
      var newFrame = currentDayView.frame
      let diameter = min(newFrame.width, newFrame.height) - 10
      newFrame.origin.x += (newFrame.width - diameter) / 2
      newFrame.origin.y += (newFrame.height - diameter) / 2
      newFrame.size.width = diameter
      newFrame.size.height = diameter
      persistedDayView.frame = newFrame
      persistedDayView.layer.cornerRadius = diameter / 2
      currentDayView.superview?.addSubview(persistedDayView)
      let label = UILabel()
      label.font = UIFont.iuBlackFont(18)
      label.textAlignment = .Center
      label.textColor = UIColor.whiteColor()
      label.text = String(currentDayView.date.day)
      label.frame = persistedDayView.bounds
      persistedDayView.addSubview(label)
      selectedDayViews.append(persistedDayView)
    }
  }
  
  func saveButtonTapped() {
    let loadingView = startLoading()
    
    useClassesViewModel.selectedDates.append(selectedDate)
    useClassesViewModel.useClasses { [weak self] inner in
      guard let weakSelf = self else {
        return
      }
      weakSelf.endLoading(loadingView)
      weakSelf.okButton.userInteractionEnabled = true
      do {
        try inner()
        if weakSelf.useClassesViewModel.studentViewModel.newStudent {
          weakSelf.delegate?.invite(weakSelf.useClassesViewModel.studentViewModel.student["name"] as? String ?? "")
        } else {
          weakSelf.delegate?.didSave()
        }
      } catch let error as NSError {
        AlertView.presentError(error.localizedDescription, buttons: nil, inView: weakSelf)
      }
    }
  }
}

// MARK: Calendar Menu View Delegate
extension UseClassesView: CVCalendarMenuViewDelegate {
  func firstWeekday() -> Weekday {
    return .Monday
  }
  
  func dayOfWeekTextColor() -> UIColor {
    return UIColor.iuGreyishColor()
  }
  
  func dayOfWeekTextUppercase() -> Bool {
    return false
  }
  
  func dayOfWeekFont() -> UIFont {
    return UIFont.iuBlackFont(18) ?? UIFont.boldSystemFontOfSize(18)
  }
  
  func weekdaySymbolType() -> WeekdaySymbolType {
    return .Short
  }
}

// MARK: Calendar View Delegate
extension UseClassesView: CVCalendarViewDelegate {
  func presentationMode() -> CalendarMode {
    return .MonthView
  }
  
  func shouldAutoSelectDayOnMonthChange() -> Bool {
    return false
  }
  
  func didShowPreviousMonthView(date: NSDate) {
    monthLabel.text = dateFormatter.stringFromDate(date).capitalizedString
    var circleFrame = animatableCircle.frame
    circleFrame.origin.x += calendarView.bounds.width
    animatableCircle.frame = circleFrame
  }
  
  func didShowNextMonthView(date: NSDate) {
    monthLabel.text = dateFormatter.stringFromDate(date).capitalizedString
    var circleFrame = animatableCircle.frame
    circleFrame.origin.x -= calendarView.bounds.width
    animatableCircle.frame = circleFrame
  }
  
  func didSelectDayView(dayView: DayView, animationDidFinish: Bool) {
    guard let date = dayView.date.convertedDate() else {
      return
    }
    selectedDate = date
    currentDayView = dayView
  }
}

// MARK: Calendar View Appearance Delegate
extension UseClassesView: CVCalendarViewAppearanceDelegate {
  func spaceBetweenDayViews() -> CGFloat {
    return 0
  }
  
  func spaceBetweenWeekViews() -> CGFloat {
    return 0
  }
  
  func dayLabelWeekdayFont() -> UIFont {
    return UIFont.iuBlackFont(18) ?? UIFont.boldSystemFontOfSize(18)
  }

  func dayLabelWeekdayInTextColor() -> UIColor {
    return UIColor.iuLightGreyColor()
  }
  
  func dayLabelPresentWeekdayTextColor() -> UIColor {
    return UIColor.iuUglyPinkColor()
  }
  
  func dayLabelWeekdaySelectedBackgroundColor() -> UIColor {
    return UIColor.iuSeafoamBlueColor()
  }
  
  func dayLabelWeekdaySelectedBackgroundAlpha() -> CGFloat {
    return 1
  }
  
  func dayLabelPresentWeekdaySelectedBackgroundColor() -> UIColor {
    return UIColor.iuSeafoamBlueColor()
  }
}

// MARK: Calendar View Animator Delegate
extension UseClassesView: CVCalendarViewAnimatorDelegate {
  func selectionAnimation() -> ((DayView, ((Bool) -> ())) -> ()) {
    return { [weak self] dayView, completion in
      if let weakSelf = self, selectionView = dayView.selectionView {
        var newFrame = weakSelf.calendarView.convertRect(selectionView.frame, fromView: selectionView.superview)
        let diameter = min(newFrame.width, newFrame.height) - 10
        newFrame.origin.x += (newFrame.width - diameter) / 2
        newFrame.origin.y += (newFrame.height - diameter) / 2
        newFrame.size.width = diameter
        newFrame.size.height = diameter
        if weakSelf.animatableCircle.layer.cornerRadius == 0 {
          weakSelf.animatableCircle.layer.cornerRadius = diameter / 2
        } else {
          weakSelf.animatableCircle.layer.cornerRadius = diameter / 2
          dayView.selectionView?.hidden = true
          weakSelf.calendarView.insertSubview(weakSelf.animatableCircle, atIndex: 0)
          UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: Constants.defaultAnimationDelay, usingSpringWithDamping: Constants.defaultAnimationDamping, initialSpringVelocity: Constants.defaultAnimationInitialVelocity, options: .CurveEaseInOut, animations: {
            weakSelf.animatableCircle.frame = newFrame
            }, completion: { finished in
              weakSelf.animatableCircle.removeFromSuperview()
              dayView.selectionView?.hidden = false
              completion(finished)
          })
        }
      }
    }
  }
  
  func deselectionAnimation() -> ((DayView, ((Bool) -> ())) -> ()) {
    return { [weak self] dayView, completion in
      if let weakSelf = self, selectionView = dayView.selectionView {
        var oldFrame = weakSelf.calendarView.convertRect(selectionView.frame, fromView: selectionView.superview)
        let diameter = min(oldFrame.width, oldFrame.height) - 10
        oldFrame.origin.x += (oldFrame.width - diameter) / 2
        oldFrame.origin.y += (oldFrame.height - diameter) / 2
        oldFrame.size.width = diameter
        oldFrame.size.height = diameter
        weakSelf.animatableCircle.frame = oldFrame
        UIView.animateWithDuration(0, animations: {}, completion: completion)
      }
    }
  }
}

// MARK: CollectionView DataSource
extension UseClassesView: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    guard let count = classesGivenViewModel?.classes.count else {
      collectionViewHeightConstraint?.constant = 0
      var contentInset = scrollView.contentInset
      contentInset.bottom = 0
      scrollView.contentInset = contentInset
      scrollView.scrollIndicatorInsets = contentInset
      UIView.animateWithDuration(Constants.defaultAnimationDuration) { [weak self] in
        self?.selectedDatesView.layoutIfNeeded()
      }
      return 0
    }
    let lines = Int((count - 1) / 3) + 1
    let height = CGFloat(lines * 20) + 20 + 20
    collectionViewHeightConstraint?.constant = height
    var contentInset = scrollView.contentInset
    contentInset.bottom = height
    scrollView.contentInset = contentInset
    scrollView.scrollIndicatorInsets = contentInset
    let visibleRect = CGRect(x: 0, y: scrollView.contentSize.height - scrollView.bounds.height, width: scrollView.bounds.width, height: scrollView.bounds.height)
    scrollView.scrollRectToVisible(visibleRect, animated: true)
    UIView.animateWithDuration(Constants.defaultAnimationDuration) { [weak self] in
      self?.selectedDatesView.layoutIfNeeded()
    }
    return count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    guard let configurator = classesGivenViewModel?.classes[indexPath.item] else {
      return UICollectionViewCell()
    }
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath)
    configurator.update(cell)
    cell.backgroundColor = UIColor.clearColor()
    return cell
  }
}

// MARK: CollectionView Delegate Flow Layout
extension UseClassesView: UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    let itemsInLine = numberOfItemsInLine(indexPath)
    let padding = (itemsInLine - 1) * 8 + 20 + 20
    let width = floor((collectionView.bounds.width - padding) / CGFloat(min(3, itemsInLine)))
    return CGSize(width: width, height: 12)
  }
  
  func numberOfItemsInLine(indexPath: NSIndexPath) -> CGFloat {
    guard let count = classesGivenViewModel?.classes.count where count > 0 else {
      return 0
    }
    let lines = Int((count - 1) / 3) + 1
    let line = Int(indexPath.item / 3) + 1
    if line != lines {
      return 3
    } else {
      let rest = count % 3
      if rest == 0 {
        return 3
      } else {
        return CGFloat(rest)
      }
    }
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    guard var classes = classesGivenViewModel?.classes where classes.count == selectedDayViews.count else {
      return
    }
    classes.removeAtIndex(indexPath.item)
    let persistedDayView = selectedDayViews[indexPath.item]
    persistedDayView.removeFromSuperview()
    selectedDayViews.removeAtIndex(indexPath.item)
    useClassesViewModel.selectedDates.removeAtIndex(indexPath.item)
    classesGivenViewModel = ClassesGivenViewModel(classes: classes)
    collectionView.reloadData()
  }
}
