//
//  LoginView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol LoginViewDelegate: class {
  func loginViewDidLogin(loginView: LoginView?)
  func signupButtonPressedOnLoginView(loginView: LoginView?)
}

final class LoginView: UIView {
  private let margin: CGFloat = 24
  private let textFieldPadding: CGFloat = 12
  
  private let emailTextField = UITextField()
  private let passwordTextField = UITextField()
  private let loginButton = UIButton(type: .Custom)
  private let signupButton = UIButton(type: .Custom)
  private let forgotButton = UIButton(type: .Custom)
  private let container = UIView()
  
  private let loginViewModel = LoginViewModel()
  
  weak var delegate: LoginViewDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    emailTextField.translatesAutoresizingMaskIntoConstraints = false
    emailTextField.placeholder = Strings.LoginTypeEmail
    emailTextField.font = UIFont.iuHeavyFont(16)
    emailTextField.textColor = UIColor.iuLightGreyColor()
    emailTextField.tintColor = UIColor.iuLightBlueGreyColor()
    emailTextField.backgroundColor = UIColor.whiteColor()
    emailTextField.keyboardType = .EmailAddress
    emailTextField.autocapitalizationType = .None
    emailTextField.autocorrectionType = .No
    emailTextField.layer.cornerRadius = 8
    emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    emailTextField.leftViewMode = .Always
    emailTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    emailTextField.rightViewMode = .Always
    emailTextField.delegate = self
    
    passwordTextField.translatesAutoresizingMaskIntoConstraints = false
    passwordTextField.placeholder = Strings.LoginTypePassword
    passwordTextField.font = UIFont.iuHeavyFont(16)
    passwordTextField.textColor = UIColor.iuLightGreyColor()
    passwordTextField.tintColor = UIColor.iuLightBlueGreyColor()
    passwordTextField.backgroundColor = UIColor.whiteColor()
    passwordTextField.secureTextEntry = true
    passwordTextField.layer.cornerRadius = 8
    passwordTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    passwordTextField.leftViewMode = .Always
    passwordTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    passwordTextField.rightViewMode = .Always
    passwordTextField.delegate = self
    
    loginButton.translatesAutoresizingMaskIntoConstraints = false
    loginButton.backgroundColor = UIColor.iuUglyPinkColor()
    loginButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    loginButton.titleLabel?.font = UIFont.iuBlackFont(18)
    loginButton.setTitle(Strings.LoginButton, forState: .Normal)
    loginButton.addTarget(self, action: #selector(LoginView.loginButtonPressed), forControlEvents: .TouchUpInside)
    loginButton.layer.cornerRadius = 8
    
    let signupContainer = UIView()
    signupContainer.translatesAutoresizingMaskIntoConstraints = false
    signupContainer.backgroundColor = UIColor.clearColor()
    
    let signupLabel = UILabel()
    signupLabel.translatesAutoresizingMaskIntoConstraints = false
    signupLabel.text = Strings.LoginSignupLabel
    signupLabel.textAlignment = .Center
    signupLabel.textColor = UIColor.iuLightGreyColor()
    signupLabel.font = UIFont.iuBlackFont(14)
    
    signupButton.translatesAutoresizingMaskIntoConstraints = false
    signupButton.setTitle(Strings.LoginSignupButton, forState: .Normal)
    signupButton.titleLabel?.textAlignment = .Center
    signupButton.setTitleColor(UIColor.iuSeafoamBlueColor(), forState: .Normal)
    signupButton.titleLabel?.font = UIFont.iuBlackFont(14)
    signupButton.addTarget(self, action: #selector(LoginView.signupButtonPressed), forControlEvents: .TouchUpInside)
    
    let forgotContainer = UIView()
    forgotContainer.translatesAutoresizingMaskIntoConstraints = false
    forgotContainer.backgroundColor = UIColor.clearColor()
    
    let forgotLabel = UILabel()
    forgotLabel.translatesAutoresizingMaskIntoConstraints = false
    forgotLabel.text = Strings.LoginForgotLabel
    forgotLabel.textAlignment = .Center
    forgotLabel.textColor = UIColor.iuLightGreyColor()
    forgotLabel.font = UIFont.iuBlackFont(14)
    
    forgotButton.translatesAutoresizingMaskIntoConstraints = false
    forgotButton.setTitle(Strings.LoginForgotButton, forState: .Normal)
    forgotButton.titleLabel?.textAlignment = .Center
    forgotButton.setTitleColor(UIColor.iuSeafoamBlueColor(), forState: .Normal)
    forgotButton.titleLabel?.font = UIFont.iuBlackFont(14)
    forgotButton.addTarget(self, action: #selector(LoginView.forgotButtonPressed), forControlEvents: .TouchUpInside)
    
    container.translatesAutoresizingMaskIntoConstraints = false
    container.addSubview(emailTextField)
    container.addSubview(passwordTextField)
    container.addSubview(loginButton)
    container.addSubview(signupContainer)
    signupContainer.addSubview(signupLabel)
    signupContainer.addSubview(signupButton)
    container.addSubview(forgotContainer)
    forgotContainer.addSubview(forgotLabel)
    forgotContainer.addSubview(forgotButton)
    addSubview(container)
    
    let views = ["emailTextField": emailTextField, "passwordTextField": passwordTextField, "loginButton": loginButton, "container": container, "signupContainer": signupContainer, "signupLabel": signupLabel, "signupButton": signupButton, "forgotContainer": forgotContainer, "forgotLabel": forgotLabel, "forgotButton": forgotButton]
    let metrics = ["margin": margin, "margin2": margin * 1.2, "height": 60]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[emailTextField]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[passwordTextField]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[loginButton]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|->=margin-[signupContainer]->=margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|->=margin-[forgotContainer]->=margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[emailTextField(height)]-margin-[passwordTextField(height)]-margin-[loginButton(height)]-margin2-[signupContainer]-margin-[forgotContainer]|", options: [], metrics: metrics, views: views)
    container.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[signupLabel]|", options: [], metrics: metrics, views: views)
    signupContainer.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[signupButton]|", options: [], metrics: metrics, views: views)
    signupContainer.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[signupLabel][signupButton]|", options: [], metrics: metrics, views: views)
    signupContainer.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[forgotLabel][forgotButton]|", options: [], metrics: metrics, views: views)
    forgotContainer.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[forgotLabel]|", options: [], metrics: metrics, views: views)
    forgotContainer.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[forgotButton]|", options: [], metrics: metrics, views: views)
    forgotContainer.addConstraints(verticalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: signupContainer, attribute: .CenterX, relatedBy: .Equal, toItem: container, attribute: .CenterX, multiplier: 1, constant: 0)]
    container.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: forgotContainer, attribute: .CenterX, relatedBy: .Equal, toItem: container, attribute: .CenterX, multiplier: 1, constant: 0)]
    container.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: container, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0)]
    addConstraints(horizontalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: container, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0)]
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[container]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|->=0-[container]->=0-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesEnded(touches, withEvent: event)
    endEditing(true)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Private
private extension LoginView {
  func startLoading() -> LoadingView {
    emailTextField.userInteractionEnabled = false
    passwordTextField.userInteractionEnabled = false
    loginButton.userInteractionEnabled = false
    signupButton.userInteractionEnabled = false
    forgotButton.userInteractionEnabled = false
    
    let loadingView = LoadingView()
    loadingView.frame = loginButton.frame
    loadingView.alpha = 0
    loginButton.superview?.addSubview(loadingView)
    UIView.animateWithDuration(Constants.defaultAnimationDuration) { [weak self] in
      loadingView.alpha = 1
      self?.loginButton.alpha = 0
    }
    return loadingView
  }
  
  func endLoading(loadingView: LoadingView) {
    dispatch_async(dispatch_get_main_queue()) {
      loadingView.endAnimating()
      UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: 1, options: [], animations: { [weak self] in
        loadingView.alpha = 0
        self?.loginButton.alpha = 1
        }, completion: { [weak self] _ in
          self?.emailTextField.userInteractionEnabled = true
          self?.passwordTextField.userInteractionEnabled = true
          self?.loginButton.userInteractionEnabled = true
          self?.signupButton.userInteractionEnabled = true
          self?.forgotButton.userInteractionEnabled = true
          loadingView.removeFromSuperview()
        })
    }
  }
}

// MARK: Actions
extension LoginView {
  func loginButtonPressed() {
    endEditing(true)
    let loadingView = startLoading()
    loginViewModel.login(emailTextField.text, password: passwordTextField.text) { [weak self] inner in
      self?.endLoading(loadingView)
      do {
        try inner()
        self?.delegate?.loginViewDidLogin(self)
      } catch let error as LoginError {
        AlertView.presentError(error.description(), buttons: nil, inView: self)
      } catch let error as NSError {
        AlertView.presentError(error.localizedDescription, buttons: nil, inView: self)
      }
    }
  }
  
  func signupButtonPressed() {
    delegate?.signupButtonPressedOnLoginView(self)
  }
  
  func forgotButtonPressed() {
    loginViewModel.forgotPassword(emailTextField.text) { [weak self] inner in
      do {
        try inner()
        AlertView.presentError(LoginError.EmailSent.description(), buttons: nil, inView: self)
      } catch {
        AlertView.presentError(LoginError.EmailNotFound.description(), buttons: nil, inView: self)
      }
    }
  }
}

// MARK: Text Field Delegate
extension LoginView: UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    switch textField {
    case emailTextField:
      passwordTextField.becomeFirstResponder()
    default:
      passwordTextField.resignFirstResponder()
    }
    return true
  }
}
