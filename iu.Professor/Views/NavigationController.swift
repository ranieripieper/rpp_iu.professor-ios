//
//  NavigationController.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol NavigationControllerDelegate: class {
  func goToLogin()
  func goToHome()
}

protocol NavigationControllerProtocol {
  weak var navigationDelegate: NavigationControllerDelegate? { get set }
}

final class NavigationController: UINavigationController {
  var modelTransitioningDelegate: TourModelTransitioningDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  init() {
    super.init(navigationBarClass: NavigationBar.self, toolbarClass: nil)
    view.backgroundColor = UIColor.iuAlmostWhiteColor()
    
    let viewController: UIViewController
    if ParseHelper.isLoggedIn() {
      let homeViewController = HomeViewController()
      homeViewController.navigationDelegate = self
      viewController = homeViewController
    } else {
      let loginViewController = LoginViewController()
      loginViewController.navigationDelegate = self
      viewController = loginViewController
    }
    addChildViewController(viewController)
    viewControllers = [viewController]
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    VersionChecker.hasUpdate { [weak self] tuple in
      if tuple.update, let weakSelf = self, view = weakSelf.view {
        AlertView.presentVersionAlert(inView: view, topPadding: weakSelf.navigationBar.bounds.height, belowSubview: weakSelf.navigationBar, mandatory: tuple.force)
      }
    }
  }
  
  override func pushViewController(viewController: UIViewController, animated: Bool) {
    topViewController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    super.pushViewController(viewController, animated: animated)
  }
}

// MARK: Public
extension NavigationController {
  func dropPro() {
    NSNotificationCenter.defaultCenter().removeObserver(self, name: Constants.dropProImageViewNotificationName, object: nil)
    guard let navigationBar = navigationBar as? NavigationBar else {
      return
    }
    
    navigationBar.dropPro()
  }
}

// MARK: Navigation Controller Delegate
extension NavigationController: NavigationControllerDelegate {
  func goToLogin() {
    dispatch_async(dispatch_get_main_queue()) { [weak self] in
      UIView.animateWithDuration(Constants.defaultAnimationDuration, animations: {
        self?.viewControllers.forEach {
          $0.view.alpha = 0
        }
        }, completion: { finished in
          self?.viewControllers.forEach {
            $0.removeFromParentViewController()
            $0.view.removeFromSuperview()
          }
          let loginViewController = LoginViewController()
          loginViewController.navigationDelegate = self
          self?.addChildViewController(loginViewController)
          self?.viewControllers = [loginViewController]
      })
    }
  }
  
  func goToHome() {
    dispatch_async(dispatch_get_main_queue()) { [weak self] in
      UIView.animateWithDuration(Constants.defaultAnimationDuration, animations: {
        self?.viewControllers.forEach {
          $0.view.alpha = 0
        }
      }, completion: { finished in
        self?.viewControllers.forEach {
          $0.removeFromParentViewController()
          $0.view.removeFromSuperview()
        }
        let homeViewController = HomeViewController()
        homeViewController.navigationDelegate = self
        self?.addChildViewController(homeViewController)
        self?.viewControllers = [homeViewController]
      })
    }
  }
}
