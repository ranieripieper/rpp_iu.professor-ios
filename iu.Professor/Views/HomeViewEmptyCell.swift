//
//  HomeViewEmptyCell.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/24/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class HomeViewEmptyCell: UITableViewCell {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    selectionStyle = .None
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Reuse Identifier
extension HomeViewEmptyCell: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return NSStringFromClass(classForCoder())
  }
}
