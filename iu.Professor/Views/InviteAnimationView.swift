//
//  InviteAnimationView.swift
//  Iu
//
//  Created by Gilson Gil on 4/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class InviteAnimationView: UIImageView, WiggleProtocol {
  private var timer: NSTimer?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    
    image = UIImage.iuEnvelop1()
    animationImages = [UIImage.iuEnvelop1()!, UIImage.iuEnvelop2()!, UIImage.iuEnvelop3()!, UIImage.iuEnvelop4()!]
    contentMode = .Bottom
    animationDuration = 1
    animationRepeatCount = 1
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(1 * NSEC_PER_SEC)), dispatch_get_main_queue()) { [weak self] in
      self?.image = UIImage.iuEnvelop4()
      self?.startAnimating()
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(2 * NSEC_PER_SEC)), dispatch_get_main_queue()) { [weak self] in
        if let weakSelf = self {
          weakSelf.wiggle()
          weakSelf.timer = NSTimer.scheduledTimerWithTimeInterval(5, target: weakSelf, selector: #selector(InviteAnimationView.aWiggle), userInfo: nil, repeats: true)
        }
      }
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Public
extension InviteAnimationView {
  func aWiggle() {
    wiggle()
  }
}
