//
//  TourViewController.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/6/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TourViewController: UIViewController {
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  override func loadView() {
    let tourView = TourView()
    tourView.delegate = self
    view = tourView
    tourView.firstPage()
    AnswersHelper.pageView(Constants.answersPageViewTour)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Tour View Delegate
extension TourViewController: TourViewDelegate {
  func dismiss() {
    NSNotificationCenter.defaultCenter().postNotificationName(Constants.tourDidFinishNotificationName, object: nil)
  }
}
