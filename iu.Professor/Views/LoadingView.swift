//
//  LoadingView.swift
//  Iu
//
//  Created by Gilson Gil on 4/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LoadingView: UIView {
  private static let innerCircleSize = CGSize(width: 5, height: 5)
  private static let outerCircleSize = CGSize(width: 10, height: 10)
  private let innerOrbitSize = CGSize(width: 14, height: 14)
  private let outerOrbitSize = CGSize(width: 60, height: 60)
  private let innerOrbitDuration = NSTimeInterval(1.5)
  private let outerOrbitDuration = NSTimeInterval(1)
  private let innerClockwise = false
  private let outerClockwise = true
  
  private let innerCircle = layer(innerCircleSize, color: UIColor.iuLightBlueGreyColor())
  private let outerCircle = layer(outerCircleSize, color: UIColor.iuLightBlueGreyColor())
  
  override var frame: CGRect {
    didSet {
      endAnimating()
      startAnimating()
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Animation
extension LoadingView {
  func startAnimating() {
    let center = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
    
    let outerAnimation = rotateAnimation(center, size: outerOrbitSize, duration: outerOrbitDuration, clockwise: outerClockwise)
    outerCircle.addAnimation(outerAnimation, forKey: "animation")
    outerCircle.opacity = 1
    layer.addSublayer(outerCircle)
    
    let innerAnimation = rotateAnimation(center, size: innerOrbitSize, duration: innerOrbitDuration, clockwise: innerClockwise)
    innerCircle.addAnimation(innerAnimation, forKey: "animation")
    innerCircle.opacity = 1
    layer.addSublayer(innerCircle)
  }
  
  func endAnimating() {
    innerCircle.removeAllAnimations()
    innerCircle.opacity = 0
    
    outerCircle.removeAllAnimations()
    outerCircle.opacity = 0
  }
}

// MARK: Private
private extension LoadingView {
  func rotateAnimation(center: CGPoint, size: CGSize, duration: NSTimeInterval, clockwise: Bool) -> CAAnimationGroup {
    let duration: CFTimeInterval = duration
    let timeFunc = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
    
    let startAngle: CGFloat
    let endAngle: CGFloat
    if clockwise {
      startAngle = -CGFloat(M_PI_2)
      endAngle = 3 * CGFloat(M_PI_2)
    } else {
      startAngle = 3 * CGFloat(M_PI_2)
      endAngle = -CGFloat(M_PI_2)
    }
    
    // Position animation
    let positionAnimation = CAKeyframeAnimation(keyPath: "position")
    positionAnimation.duration = duration
    positionAnimation.repeatCount = HUGE
    positionAnimation.path = UIBezierPath(arcCenter: center, radius: size.width / 4, startAngle: startAngle, endAngle: endAngle, clockwise: clockwise).CGPath
    
    // Aniamtion
    let animation = CAAnimationGroup()
    animation.animations = [positionAnimation]
    animation.timingFunction = timeFunc
    animation.duration = duration
    animation.repeatCount = HUGE
    animation.removedOnCompletion = false
    
    return animation
  }
  
  static func layer(size: CGSize, color: UIColor) -> CALayer {
    let layer: CAShapeLayer = CAShapeLayer()
    let path: UIBezierPath = UIBezierPath()
    
    let center = CGPoint(x: size.width / 2, y: size.height / 2)
    let radius = size.width / 2
    let startAngle = CGFloat(0)
    let endAngle = CGFloat(2 * M_PI)
    let clockwise = false
    
    path.addArcWithCenter(center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: clockwise)
    layer.fillColor = color.CGColor
    layer.backgroundColor = nil
    layer.path = path.CGPath
    
    return layer
  }
}
