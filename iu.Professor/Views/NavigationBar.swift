//
//  NavigationBar.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class NavigationBar: UINavigationBar {
  private static let dropProAnimationDelay: NSTimeInterval = 0.4
  
  private let proImageView = UIImageView(image: UIImage.iuPro())
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    barTintColor = UIColor.iuDarkGreyColor()
    tintColor = UIColor.whiteColor()
    translucent = false
    
    let logoImage = UIImage.iuLogo()
    let logoImageView = UIImageView(image: logoImage)
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(logoImageView)
    
    proImageView.translatesAutoresizingMaskIntoConstraints = false
    proImageView.alpha = 0
    addSubview(proImageView)
    
    let logoConstraints = [
      NSLayoutConstraint(item: self, attribute: .RightMargin, relatedBy: .Equal, toItem: logoImageView, attribute: .Right, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: self, attribute: .CenterY, relatedBy: .Equal, toItem: logoImageView, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    addConstraints(logoConstraints)
    
    let proConstraints = [
      NSLayoutConstraint(item: self, attribute: .Right, relatedBy: .Equal, toItem: proImageView, attribute: .Right, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: self, attribute: .Top, relatedBy: .Equal, toItem: proImageView, attribute: .Top, multiplier: 1, constant: 0)
    ]
    addConstraints(proConstraints)
  }
  
  override func sizeThatFits(size: CGSize) -> CGSize {
    return CGSize(width: UIScreen.mainScreen().bounds.width, height: Constants.navigationBarHeight)
  }
}

extension NavigationBar {
  func dropPro() {
    proImageView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -proImageView.bounds.height)
    proImageView.alpha = 1
    UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: NavigationBar.dropProAnimationDelay, usingSpringWithDamping: Constants.defaultAnimationDamping, initialSpringVelocity: Constants.defaultAnimationInitialVelocity, options: .CurveEaseIn, animations: { [weak self] in
      self?.proImageView.transform = CGAffineTransformIdentity
      }, completion: nil)
  }
}
