//
//  TourView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol TourViewDelegate: class {
  func dismiss()
}

final class TourView: UIView {
  private var currentPage = 0
  private let container = UIView()
  let button = TourButton()
  
  weak var delegate: TourViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuDarkGreyColor()
    
    button.translatesAutoresizingMaskIntoConstraints = false
    button.addTarget(self, action: #selector(TourView.nextPage), forControlEvents: .TouchUpInside)
    addSubview(button)
    
    container.translatesAutoresizingMaskIntoConstraints = false
    container.backgroundColor = UIColor.iuDarkGreyColor()
    addSubview(container)
    
    let metrics = ["buttonHeight": Constants.tourViewButtonHeight]
    let views = ["container": container, "button": button]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[container]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[button]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[container][button(buttonHeight)]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
  }
}

// MARK: Public
extension TourView {
  func firstPage() {
    button.tourState = .Next
    currentPage = 1
    goToPage1()
  }
  
  func nextPage() {
    goToPage(currentPage + 1)
  }
}

// MARK: Private
private extension TourView {
  func goToPage(page: Int) {
    currentPage = page
    container.subviews.forEach {
      if let tourPageView = $0 as? TourPageViewProtocol {
        tourPageView.fadeOut { [weak self] in
          switch page {
          case 2:
            self?.button.tourState = .Next
            self?.goToPage2()
          case 3:
            self?.button.tourState = .Next
            self?.goToPage3()
          case 4:
            self?.button.tourState = .Next
            self?.goToPage4()
          case 5:
            self?.button.tourState = .Last
            self?.goToPage5()
          case 6:
            self?.button.tourState = .None
            self?.goToPage6()
          default:
            break
          }
        }
      }
    }
  }
  
  func goToPage1() {
    let tourPage1View = TourPage1View()
    container.addSubview(tourPage1View)
    addConstraintsToView(tourPage1View)
  }
  
  func goToPage2() {
    let tourPage2View = TourPage2View()
    container.addSubview(tourPage2View)
    addConstraintsToView(tourPage2View)
  }
  
  func goToPage3() {
    let tourPage3View = TourPage3View()
    container.addSubview(tourPage3View)
    addConstraintsToView(tourPage3View)
  }
  
  func goToPage4() {
    let tourPage4View = TourPage4View()
    container.addSubview(tourPage4View)
    addConstraintsToView(tourPage4View)
  }
  
  func goToPage5() {
    let tourPage5View = TourPage5View()
    container.addSubview(tourPage5View)
    addConstraintsToView(tourPage5View)
  }
  
  func goToPage6() {
    let tourPage6View = TourPage6View()
    container.addSubview(tourPage6View)
    addConstraintsToView(tourPage6View)
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC * 1)), dispatch_get_main_queue()) { [weak self] in
      self?.delegate?.dismiss()
    }
  }
  
  func addConstraintsToView(view: UIView) {
    let views = ["view": view]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options: [], metrics: nil, views: views)
    container.addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]|", options: [], metrics: nil, views: views)
    container.addConstraints(verticalConstraints)
  }
}
