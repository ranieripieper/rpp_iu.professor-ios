//
//  ClassesGivenView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ClassesGivenView: UIView {
  private let headerLabel = UILabel()
  private let footerLabel = UILabel()
  private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: DatesFlowLayout(style: .Dark))
  private var collectionViewHeightConstraint: NSLayoutConstraint!
  
  var viewModel: ClassesGivenViewModel? {
    didSet {
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        if let weakSelf = self {
          weakSelf.headerLabel.hidden = !(weakSelf.viewModel?.classes.count > 0)
          weakSelf.footerLabel.hidden = !(weakSelf.viewModel?.classes.count > 0)
          weakSelf.viewModel?.classes.forEach {
            weakSelf.collectionView.registerClass($0.cellClass, forCellWithReuseIdentifier: $0.reuseIdentifier)
          }
          weakSelf.collectionView.reloadData()
        }
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(viewModel: ClassesGivenViewModel?) {
    self.viewModel = viewModel
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    headerLabel.translatesAutoresizingMaskIntoConstraints = false
    headerLabel.textAlignment = .Center
    headerLabel.textColor = UIColor.iuLightBlueGreyTwoColor()
    headerLabel.text = Strings.ClassesGiven
    headerLabel.font = UIFont.iuBlackFont(16)
    addSubview(headerLabel)
    
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    collectionView.backgroundColor = UIColor.iuAlmostWhiteColor()
    collectionView.dataSource = self
    collectionView.delegate = self
    addSubview(collectionView)
    
    footerLabel.translatesAutoresizingMaskIntoConstraints = false
    footerLabel.textAlignment = .Center
    footerLabel.textColor = UIColor.iuLightBlueGreyTwoColor()
    footerLabel.text = Strings.ClassesSeeMore
    footerLabel.font = UIFont.iuBlackFont(16)
    addSubview(footerLabel)
    
    let views = ["headerLabel": headerLabel, "collectionView": collectionView, "footerLabel": footerLabel]
    
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[headerLabel]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[collectionView(280)]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[footerLabel]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[headerLabel]-[collectionView]-[footerLabel]|", options: [], metrics: nil, views: views)
    addConstraints(verticalConstraints)
    collectionViewHeightConstraint = NSLayoutConstraint(item: collectionView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 0)
    collectionViewHeightConstraint.priority = UILayoutPriorityDefaultHigh
    collectionView.addConstraint(collectionViewHeightConstraint)
    
    viewModel?.classes.forEach {
      self.collectionView.registerClass($0.cellClass, forCellWithReuseIdentifier: $0.reuseIdentifier)
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: CollectionView DataSource
extension ClassesGivenView: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    let count = viewModel?.classes.count ?? 0
    let lines = Int((count - 1) / 3) + 1
    collectionViewHeightConstraint.constant = CGFloat(lines * 20)
    UIView.animateWithDuration(Constants.defaultAnimationDuration) { [weak self] in
      self?.layoutIfNeeded()
    }
    footerLabel.hidden = count < 6
    return count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    guard let configurator = viewModel?.classes[indexPath.item] else {
      return UICollectionViewCell()
    }
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath)
    configurator.update(cell)
    return cell
  }
}

// MARK: CollectionView Delegate Flow Layout
extension ClassesGivenView: UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    let itemsInLine = numberOfItemsInLine(indexPath)
    let padding = (itemsInLine - 1) * 8
    let width = floor((collectionView.bounds.width - padding) / CGFloat(min(3, itemsInLine)))
    return CGSize(width: width, height: 12)
  }
  
  func numberOfItemsInLine(indexPath: NSIndexPath) -> CGFloat {
    guard let count = viewModel?.classes.count where count > 0 else {
      return 0
    }
    let lines = Int((count - 1) / 3) + 1
    let line = Int(indexPath.item / 3) + 1
    if line != lines {
      return 3
    } else {
      let rest = count % 3
      if rest == 0 {
        return 3
      } else {
        return CGFloat(rest)
      }
    }
  }
}
