//
//  TourPage2View.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TourPage2View: UIView, TourPageViewProtocol {
  private static let numberOfCircles = 2
  private static let circle1Width: CGFloat = 24
  private static let circle2Width: CGFloat = 20
  private static let circle3Width: CGFloat = 14
  private static let circle4Width: CGFloat = 8

  private let listLabel = UILabel()
  private let createLabel = UILabel()
  private let detailImageView = PulseImageView()
  private let shadowImageView = UIImageView()
  private let circle1 = CircleLayerView(circles: TourPage2View.numberOfCircles, width: TourPage2View.circle1Width)
  private let circle2 = CircleLayerView(circles: TourPage2View.numberOfCircles, width: TourPage2View.circle2Width)
  private let circle3 = CircleLayerView(circles: TourPage2View.numberOfCircles, width: TourPage2View.circle3Width)
  private let circle4 = CircleLayerView(circles: TourPage2View.numberOfCircles, width: TourPage2View.circle4Width)
  private let studentLabel = UILabel()
  
  var labels: [UIView] = []
  var images: [UIView] = []
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.iuDarkGreyColor()
    
    listLabel.translatesAutoresizingMaskIntoConstraints = false
    listLabel.font = UIFont.iuBlackFont(36)
    listLabel.text = Strings.Tour2ListLabelText
    listLabel.textColor = UIColor.whiteColor()
    listLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(listLabel)
    
    createLabel.translatesAutoresizingMaskIntoConstraints = false
    createLabel.font = UIFont.iuBlackFont(14)
    createLabel.text = Strings.Tour2CreateLabelText
    createLabel.textColor = UIColor.whiteColor()
    createLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(createLabel)
    
    shadowImageView.translatesAutoresizingMaskIntoConstraints = false
    shadowImageView.image = UIImage.iuShadow()
    shadowImageView.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    shadowImageView.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(shadowImageView)
    
    circle1.translatesAutoresizingMaskIntoConstraints = false
    circle1.alpha = 0
    addSubview(circle1)
    
    circle2.translatesAutoresizingMaskIntoConstraints = false
    circle2.alpha = 0
    addSubview(circle2)
    
    circle3.translatesAutoresizingMaskIntoConstraints = false
    circle3.alpha = 0
    addSubview(circle3)
    
    circle4.translatesAutoresizingMaskIntoConstraints = false
    circle4.alpha = 0
    addSubview(circle4)
    
    detailImageView.translatesAutoresizingMaskIntoConstraints = false
    detailImageView.animationImages = UIImage.iuTourListImages()
    detailImageView.setContentCompressionResistancePriority(UILayoutPriorityDefaultHigh, forAxis: .Vertical)
    detailImageView.contentMode = .ScaleAspectFit
    addSubview(detailImageView)
    
    studentLabel.translatesAutoresizingMaskIntoConstraints = false
    studentLabel.font = UIFont.iuBlackFont(14)
    studentLabel.text = Strings.Tour2NameLabelText
    studentLabel.textColor = UIColor.whiteColor()
    studentLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(studentLabel)
    
    labels = [listLabel, createLabel, studentLabel]
    images = [detailImageView, shadowImageView]
    
    let metrics = ["margin": 20]
    let views = ["listLabel": listLabel, "createLabel": createLabel, "circle1": circle1, "circle2": circle2, "circle3": circle3, "circle4": circle4, "detailImageView": detailImageView, "shadowImageView": shadowImageView, "studentLabel": studentLabel]
    [listLabel, createLabel, detailImageView, shadowImageView, studentLabel].forEach {
      let horizontalConstraints = [
        NSLayoutConstraint(item: $0, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: $0, attribute: .Left, relatedBy: .GreaterThanOrEqual, toItem: self, attribute: .LeftMargin, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: $0, attribute: .Right, relatedBy: .LessThanOrEqual, toItem: self, attribute: .RightMargin, multiplier: 1, constant: 0)
      ]
      addConstraints(horizontalConstraints)
    }
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-[listLabel]-margin-[createLabel]-margin-[detailImageView]-[shadowImageView]-margin-[studentLabel]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    let centerConstraint = NSLayoutConstraint(item: detailImageView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: Constants.tourViewButtonHeight / 2)
    addConstraint(centerConstraint)
    
    let circle1Constraints = [
      NSLayoutConstraint(item: circle1, attribute: .Left, relatedBy: .Equal, toItem: detailImageView, attribute: .Right, multiplier: 1, constant: 14),
      NSLayoutConstraint(item: circle1, attribute: .Top, relatedBy: .Equal, toItem: detailImageView, attribute: .CenterY, multiplier: 1.2, constant: 0),
      NSLayoutConstraint(item: circle1, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: TourPage2View.circle1Width),
      NSLayoutConstraint(item: circle1, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: TourPage2View.circle1Width)
    ]
    addConstraints(circle1Constraints)
    
    let circle2Constraints = [
      NSLayoutConstraint(item: circle2, attribute: .Left, relatedBy: .Equal, toItem: detailImageView, attribute: .Left, multiplier: 0.5, constant: 0),
      NSLayoutConstraint(item: circle2, attribute: .Top, relatedBy: .Equal, toItem: detailImageView, attribute: .Top, multiplier: 1, constant: 20),
      NSLayoutConstraint(item: circle2, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: TourPage2View.circle2Width),
      NSLayoutConstraint(item: circle2, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: TourPage2View.circle2Width)
    ]
    addConstraints(circle2Constraints)
    
    let circle3Constraints = [
      NSLayoutConstraint(item: circle3, attribute: .Left, relatedBy: .Equal, toItem: detailImageView, attribute: .Right, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: circle3, attribute: .Top, relatedBy: .Equal, toItem: detailImageView, attribute: .Top, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: circle3, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: TourPage2View.circle3Width),
      NSLayoutConstraint(item: circle3, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: TourPage2View.circle3Width)
    ]
    addConstraints(circle3Constraints)
    
    let circle4Constraints = [
      NSLayoutConstraint(item: circle4, attribute: .Left, relatedBy: .Equal, toItem: circle1, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: circle4, attribute: .Bottom, relatedBy: .Equal, toItem: detailImageView, attribute: .CenterY, multiplier: 0.8, constant: 0),
      NSLayoutConstraint(item: circle4, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: TourPage2View.circle4Width),
      NSLayoutConstraint(item: circle4, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: TourPage2View.circle4Width)
    ]
    addConstraints(circle4Constraints)
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    superview?.superview?.userInteractionEnabled = false
    fadeIn { [weak self] in
      guard let weakSelf = self else {
        return
      }
      weakSelf.circle1.pop()
      TourPage2View.after {
        weakSelf.circle2.pop()
        TourPage2View.after {
          weakSelf.circle3.pop()
          TourPage2View.after {
            weakSelf.circle4.pop()
            weakSelf.superview?.superview?.userInteractionEnabled = true
          }
        }
      }
      weakSelf.images.appendContentsOf([weakSelf.circle1, weakSelf.circle2, weakSelf.circle3, weakSelf.circle4])
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Private
private extension TourPage2View {
  static func after(completion: () -> ()) {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_MSEC * 500)), dispatch_get_main_queue(), completion)
  }
}
