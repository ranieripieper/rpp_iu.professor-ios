//
//  InviteViewController.swift
//  Iu
//
//  Created by Gilson Gil on 4/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class InviteViewController: UIViewController {
  private let inviteView: InviteView
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(name: String) {
    inviteView = InviteView(name: name)
    super.init(nibName: nil, bundle: nil)
    inviteView.delegate = self
  }
  
  override func loadView() {
    view = inviteView
    AnswersHelper.pageView(Constants.answersPageViewInvite)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    guard let viewControllers = navigationController?.viewControllers where viewControllers.count > 2, let first = viewControllers.first, last = viewControllers.last else {
      return
    }
    navigationController?.viewControllers = [first, last]
    navigationItem.hidesBackButton = true
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Invite View Delegate
extension InviteViewController: InviteViewDelegate {
  func inviteViewInvite() {
    guard let url = NSURL(string: Constants.iuAppStoreLink) else {
      return
    }
    let activityViewController = UIActivityViewController(activityItems: [Strings.InviteShare, url], applicationActivities: nil)
    activityViewController.completionWithItemsHandler = { media, success, _, _ in
      if success, let media = media {
        AnswersHelper.invite(media)
      }
      self.navigationController?.popToRootViewControllerAnimated(true)
    }
    presentViewController(activityViewController, animated: true, completion: nil)
  }
  
  func inviteViewDismiss() {
    navigationController?.popToRootViewControllerAnimated(true)
  }
}
