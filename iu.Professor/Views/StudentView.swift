//
//  StudentView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/23/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol StudentViewDelegate: class {
  func didSave()
  func invite(name: String)
  func studentView(studentView: StudentView, classesCount: Int, useClassesCount: Int, expireDate: NSDate, newStudent: Student?)
  func showAllClassesGiven()
}

final class StudentView: UIView {
  private let margin: CGFloat = 10
  private let textFieldPadding: CGFloat = 12
  private let hiddenAlpha: CGFloat = 0.5
  private let loadingViewSize: CGFloat = 60
  
  private let classesLabel = UILabel()
  private let counterLabel = UILabel()
  private let dateLabel = UILabel()
  private let lessButton = UIButton(type: .Custom)
  private let moreButton = UIButton(type: .Custom)
  private let dateButton = UIButton(type: .Custom)
  private let saveButton = UIButton(type: .Custom)
  private let classesGivenView: ClassesGivenView
  private var counter = 0
  private var selectedDate = NSDate()
  private var datePickerView: DatePickerView?
  private let container = UIView()
  private let loadingView = LoadingView()
  private lazy var dateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    dateFormatter.dateFormat = "dd/MM/yyyy"
    return dateFormatter
  }()
  
  private var studentViewModel: StudentViewModel!
  private var loadingCounter = 2 {
    didSet {
      if loadingCounter == 0 {
        UIView.animateWithDuration(Constants.defaultAnimationDuration, animations: { [weak self] in
          self?.container.alpha = 1
          self?.saveButton.alpha = 1
          self?.loadingView.alpha = 0
          }, completion: { [weak self] _ in
            self?.stopLoading()
          })
      }
    }
  }
  
  weak var delegate: StudentViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(studentViewModel: StudentViewModel) {
    self.studentViewModel = studentViewModel
    self.classesGivenView = ClassesGivenView(viewModel: studentViewModel.classesGivenViewModel)
    super.init(frame: .zero)
    setUp()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    if loadingCounter > 0 && loadingView.superview == nil {
      startLoading(nil)
    }
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    let scrollView = UIScrollView()
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(scrollView)
    
    classesLabel.translatesAutoresizingMaskIntoConstraints = false
    classesLabel.textAlignment = .Center
    classesLabel.textColor = UIColor.iuLightGreyColor()
    classesLabel.font = UIFont.iuBlackFont(18)
    classesLabel.numberOfLines = 0
    
    counterLabel.translatesAutoresizingMaskIntoConstraints = false
    counterLabel.text = "0"
    counterLabel.textAlignment = .Center
    counterLabel.textColor = UIColor.iuUglyPinkColor()
    counterLabel.font = UIFont.iuBlackFont(160)
    
    lessButton.translatesAutoresizingMaskIntoConstraints = false
    lessButton.setImage(UIImage.iuLess(), forState: .Normal)
    lessButton.addTarget(self, action: #selector(StudentView.lessButtonPressed), forControlEvents: .TouchUpInside)
    
    moreButton.translatesAutoresizingMaskIntoConstraints = false
    moreButton.setImage(UIImage.iuMore(), forState: .Normal)
    moreButton.addTarget(self, action: #selector(StudentView.moreButtonPressed), forControlEvents: .TouchUpInside)
    
    dateLabel.translatesAutoresizingMaskIntoConstraints = false
    dateLabel.textAlignment = .Center
    dateLabel.textColor = UIColor.iuLightGreyColor()
    dateLabel.font = UIFont.iuBlackFont(18)
    
    dateButton.translatesAutoresizingMaskIntoConstraints = false
    dateButton.titleLabel?.textAlignment = .Center
    dateButton.setTitleColor(UIColor.iuGreyishColor(), forState: .Normal)
    dateButton.titleLabel?.font = UIFont.iuBlackFont(36)
    dateButton.addTarget(self, action: #selector(StudentView.expireButtonPressed), forControlEvents: .TouchUpInside)
    
    saveButton.translatesAutoresizingMaskIntoConstraints = false
    saveButton.backgroundColor = UIColor.iuUglyPinkColor()
    saveButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    saveButton.titleLabel?.font = UIFont.iuBlackFont(18)
    saveButton.setTitle(Strings.StudentSaveButtonTitle.uppercaseString, forState: .Normal)
    saveButton.addTarget(self, action: #selector(StudentView.saveButtonPressed), forControlEvents: .TouchUpInside)
    saveButton.layer.cornerRadius = 8
    
    classesGivenView.translatesAutoresizingMaskIntoConstraints = false
    classesGivenView.viewModel = studentViewModel.classesGivenViewModel
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(StudentView.classesGivenTapped))
    classesGivenView.addGestureRecognizer(tap)
    
    container.translatesAutoresizingMaskIntoConstraints = false
    container.addSubview(saveButton)
    scrollView.addSubview(container)
    
    container.addSubview(classesLabel)
    container.addSubview(counterLabel)
    container.addSubview(lessButton)
    container.addSubview(moreButton)
    container.addSubview(dateLabel)
    container.addSubview(dateButton)
    container.addSubview(saveButton)
    container.addSubview(classesGivenView)
    container.alpha = 0
    
    let views = ["classesLabel": classesLabel, "counterLabel": counterLabel, "lessButton": lessButton, "moreButton": moreButton, "dateLabel": dateLabel, "dateButton": dateButton, "saveButton": saveButton, "classesGivenView": classesGivenView, "container": container, "scrollView": scrollView]
    let metrics = ["margin": margin, "margin2": margin * 2, "height": 60]
    [classesLabel, counterLabel, dateLabel, dateButton, saveButton].forEach {
      let centerXConstraint = NSLayoutConstraint(item: $0, attribute: .CenterX, relatedBy: .Equal, toItem: container, attribute: .CenterX, multiplier: 1, constant: 0)
      container.addConstraint(centerXConstraint)
    }
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[lessButton]-margin-[counterLabel]-margin-[moreButton]", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = [NSLayoutConstraint(item: saveButton, attribute: .Width, relatedBy: .Equal, toItem: container, attribute: .Width, multiplier: 0.8, constant: 0)]
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = [NSLayoutConstraint(item: classesGivenView, attribute: .CenterX, relatedBy: .Equal, toItem: container, attribute: .CenterX, multiplier: 1, constant: 0)]
    container.addConstraints(horizontalConstraints)
    
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin2-[classesLabel(80)][counterLabel(180)]-[dateLabel]-[dateButton]-margin2-[saveButton(height)]-margin2-[classesGivenView]->=margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(verticalConstraints)
    
    verticalConstraints = [
      NSLayoutConstraint(item: lessButton, attribute: .CenterY, relatedBy: .Equal, toItem: counterLabel, attribute: .CenterY, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: counterLabel, attribute: .CenterY, relatedBy: .Equal, toItem: moreButton, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    container.addConstraints(verticalConstraints)
    
    [lessButton, moreButton].forEach {
      let widthConstraint = NSLayoutConstraint(item: $0, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 50)
      $0.addConstraint(widthConstraint)
      let heightConstraint = NSLayoutConstraint(item: $0, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 50)
      $0.addConstraint(heightConstraint)
    }
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[container]|", options: [], metrics: metrics, views: views)
    scrollView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[container]|", options: [], metrics: metrics, views: views)
    scrollView.addConstraints(verticalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: container, attribute: .Height, relatedBy: .GreaterThanOrEqual, toItem: self, attribute: .Height, multiplier: 1, constant: 0)]
    addConstraints(verticalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: container, attribute: .Width, relatedBy: .Equal, toItem: self, attribute: .Width, multiplier: 1, constant: 0)]
    addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollView]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    configure()
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Public
extension StudentView {
  func configure() {
    switch studentViewModel.paymentType {
    case .Before(let count, _):
      if let name = studentViewModel.student["name"] as? String {
        classesLabel.text = Strings.StudentClassCountBefore.stringByReplacingOccurrencesOfString("%@", withString: name)
      }
      dateLabel.text = Strings.StudentClassExpire
      counter = count
    case .After(let count, _):
      if let name = studentViewModel.student["name"] as? String {
        classesLabel.text = String(format: Strings.StudentClassCountAfter, [name])
        classesLabel.text = Strings.StudentClassCountAfter.stringByReplacingOccurrencesOfString("%@", withString: name)
      }
      dateLabel.text = Strings.StudentClassPayment
      counter = count
    }
    counterLabel.text = String(counter)
    if let date = dateFormatter.dateFromString(studentViewModel.dateString) {
      selectedDate = date
    }
    dateButton.setTitle(studentViewModel.dateString, forState: .Normal)
    loadingCounter = 0
  }
  
  func dismissDatePicker(datePickerView: DatePickerView) {
    UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: Constants.defaultAnimationDelay, usingSpringWithDamping: Constants.animationNoDamping, initialSpringVelocity: Constants.defaultAnimationInitialVelocity, options: .CurveEaseInOut, animations: {
      datePickerView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, datePickerView.bounds.height)
      }, completion: { finished in
        datePickerView.removeFromSuperview()
        self.datePickerView?.delegate = nil
        self.datePickerView = nil
    })
  }
}

// MARK: Private
private extension StudentView {
  func startLoading(view: UIView?) {
    lessButton.userInteractionEnabled = false
    moreButton.userInteractionEnabled = false
    dateButton.userInteractionEnabled = false
    saveButton.userInteractionEnabled = false
    
    loadingView.translatesAutoresizingMaskIntoConstraints = false
    
    if let view = view {
      view.superview?.addSubview(loadingView)
      
      loadingView.alpha = 0
      let horizontalConstraints = [
        NSLayoutConstraint(item: loadingView, attribute: .Left, relatedBy: .Equal, toItem: view, attribute: .Left, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: loadingView, attribute: .Right, relatedBy: .Equal, toItem: view, attribute: .Right, multiplier: 1, constant: 0)
      ]
      view.superview?.addConstraints(horizontalConstraints)
      let verticalConstraints = [
        NSLayoutConstraint(item: loadingView, attribute: .Bottom, relatedBy: .Equal, toItem: view, attribute: .Bottom, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: loadingView, attribute: .Top, relatedBy: .Equal, toItem: view, attribute: .Top, multiplier: 1, constant: 0)
      ]
      view.superview?.addConstraints(verticalConstraints)
      UIView.animateWithDuration(Constants.defaultAnimationDuration) { [weak self] in
        view.alpha = 0
        self?.loadingView.alpha = 1
      }
    } else {
      addSubview(loadingView)
      
      let horizontalConstraints = [
        NSLayoutConstraint(item: loadingView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: loadingView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: loadingViewSize)
      ]
      addConstraints(horizontalConstraints)
      let verticalConstraints = [
        NSLayoutConstraint(item: loadingView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: loadingView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: loadingViewSize)
      ]
      addConstraints(verticalConstraints)
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(5 * NSEC_PER_MSEC)), dispatch_get_main_queue()) { [weak self] in
      self?.loadingView.startAnimating()
    }
  }
  
  func stopLoading() {
    lessButton.userInteractionEnabled = true
    moreButton.userInteractionEnabled = true
    dateButton.userInteractionEnabled = true
    saveButton.userInteractionEnabled = true
    
    loadingView.endAnimating()
    loadingView.removeFromSuperview()
  }
}

// MARK: Actions
extension StudentView {
  func lessButtonPressed() {
    self.counter = max(0, self.counter - 1)
    self.counterLabel.text = String(self.counter)
  }
  
  func moreButtonPressed() {
    self.counter = min(50, self.counter + 1)
    self.counterLabel.text = String(self.counter)
  }
  
  func expireButtonPressed() {
    if let datePickerView = datePickerView {
      dismissDatePicker(datePickerView)
    } else {
      datePickerView = DatePickerView(initialDate: selectedDate)
      datePickerView?.delegate = self
      addSubview(datePickerView!)
      let height: CGFloat = 216 + 44
      datePickerView?.frame = CGRect(x: 0, y: bounds.height - height, width: bounds.width, height: height)
      datePickerView?.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, height)
      UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: Constants.defaultAnimationDelay, usingSpringWithDamping: Constants.animationNoDamping, initialSpringVelocity: Constants.defaultAnimationInitialVelocity, options: .CurveEaseInOut, animations: {
        self.datePickerView?.transform = CGAffineTransformIdentity
        }, completion: nil)
    }
  }
  
  func saveButtonPressed() {
    loadingCounter += 1
    startLoading(saveButton)
    studentViewModel.saveStudent(counter, expireDate: selectedDate, usedDates: nil) { [weak self] inner in
      dispatch_async(dispatch_get_main_queue()) {
        guard let weakSelf = self else {
          return
        }
        weakSelf.loadingCounter = max(0, weakSelf.loadingCounter - 1)
        do {
          try inner()
          if weakSelf.studentViewModel.newStudent {
            weakSelf.delegate?.invite(weakSelf.studentViewModel.student["name"] as? String ?? "")
          } else {
            weakSelf.delegate?.didSave()
          }
        } catch let error as StudentError {
          switch error {
          case .DateParameterMissing(let count, let newStudent):
            weakSelf.delegate?.studentView(weakSelf, classesCount: weakSelf.counter, useClassesCount: count, expireDate: weakSelf.selectedDate, newStudent: newStudent)
          default:
            AlertView.presentError(error.description(), buttons: nil, inView: weakSelf)
          }
        } catch let error as NSError {
          AlertView.presentError(error.localizedDescription, buttons: nil, inView: weakSelf)
        }
      }
    }
  }
  
  func classesGivenTapped() {
    if classesGivenView.viewModel?.classes.count >= 5 {
      delegate?.showAllClassesGiven()
    }
  }
}

// MARK: DatePicker View Delegate
extension StudentView: DatePickerViewDelegate {
  func datePickerView(datePickerView: DatePickerView, didSelectDate date: NSDate) {
    selectedDate = date
    dateButton.setTitle(dateFormatter.stringFromDate(date), forState: .Normal)
    dismissDatePicker(datePickerView)
  }
}
