//
//  DatePickerView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/24/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol DatePickerViewDelegate: class {
  func datePickerView(datePickerView: DatePickerView, didSelectDate date: NSDate)
}

final class DatePickerView: UIView {
  private let pickerView = UIDatePicker()
  private let initialDate: NSDate?
  
  weak var delegate: DatePickerViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    initialDate = nil
    super.init(coder: aDecoder)
  }
  
  init() {
    initialDate = nil
    super.init(frame: .zero)
    setUp()
  }
  
  init(initialDate: NSDate) {
    self.initialDate = initialDate
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    let toolbar = UIToolbar()
    toolbar.translatesAutoresizingMaskIntoConstraints = false
    toolbar.tintColor = UIColor.iuLightBlueGreyColor()
    
    let space = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
    let okButton = UIBarButtonItem(title: Strings.DatePickerOkButtonTitle, style: .Plain, target: self, action: #selector(DatePickerView.okTapped))
    toolbar.items = [space, okButton]
    
    pickerView.translatesAutoresizingMaskIntoConstraints = false
    pickerView.datePickerMode = .Date
    pickerView.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    pickerView.locale = NSLocale(localeIdentifier: "pt-BR")
    if let initialDate = initialDate {
      pickerView.date = initialDate
    }
    
    addSubview(toolbar)
    addSubview(pickerView)
    
    let metrics = ["toolbarHeight": 44]
    let views = ["toolbar": toolbar, "pickerView": pickerView]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[toolbar]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[pickerView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[toolbar(toolbarHeight)][pickerView]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Actions
extension DatePickerView {
  func okTapped() {
    delegate?.datePickerView(self, didSelectDate: pickerView.date)
  }
}
