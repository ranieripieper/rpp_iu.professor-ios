//
//  TourPageViewProtocol.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol TourPageViewProtocol {
  var labels: [UIView] { get }
  var images: [UIView] { get }
}

extension TourPageViewProtocol {
  func delay() -> NSTimeInterval {
    return 0.2
  }
  
  func fadeIn(completion: (() -> ())?) {
    labels.forEach {
      $0.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, UIScreen.mainScreen().bounds.width, 0)
    }
    images.forEach {
      $0.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, UIScreen.mainScreen().bounds.width, 0)
    }
    UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: Constants.defaultAnimationDelay, options: .CurveEaseInOut, animations: {
      self.labels.forEach {
        $0.transform = CGAffineTransformIdentity
      }
      }, completion: nil)
    UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: delay(), options: .CurveEaseInOut, animations: {
      self.images.forEach {
        $0.transform = CGAffineTransformIdentity
      }
      }, completion: { _ in
        completion?()
    })
  }
  
  func fadeOut(completion: (() -> ())?) {
    self.images.forEach {
      $0.layer.removeAllAnimations()
      if let pulseImage = $0 as? PulseImageView {
        pulseImage.shouldAnimate = false
      }
    }
    UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: Constants.defaultAnimationDelay, options: .CurveEaseInOut, animations: {
      self.images.forEach {
        $0.transform = CGAffineTransformMakeTranslation(-UIScreen.mainScreen().bounds.width, 0)
      }
    }, completion: nil)
    UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: delay(), options: .CurveEaseInOut, animations: {
      self.labels.forEach {
        $0.transform = CGAffineTransformMakeTranslation(-UIScreen.mainScreen().bounds.width, 0)
      }
      }, completion: { _ in
        completion?()
        if let view = self as? UIView {
          view.removeFromSuperview()
        }
    })
  }
}
