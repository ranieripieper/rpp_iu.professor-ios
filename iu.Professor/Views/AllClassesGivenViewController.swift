//
//  AllClassesGivenViewController.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/30/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class AllClassesGivenViewController: UIViewController {
  private let student: Student?
  private let allClassesGivenView: AllClassesGivenView
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(student: Student) {
    allClassesGivenView = AllClassesGivenView(student: student)
    self.student = student
    super.init(nibName: nil, bundle: nil)
    allClassesGivenView.delegate = self
  }
  
  override func loadView() {
    view = allClassesGivenView
    AnswersHelper.pageView(Constants.answersPageViewHistory)
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: All Classes Given View Delegate
extension AllClassesGivenViewController: AllClassesGivenViewDelegate {
  func pop() {
    navigationController?.popViewControllerAnimated(true)
  }
}
