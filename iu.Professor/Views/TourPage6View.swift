//
//  TourPage6View.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TourPage6View: UIView, TourPageViewProtocol {
  private let byeLabel = UILabel()
  private let logoImageView = UIImageView()
  
  var labels: [UIView] = []
  var images: [UIView] = []
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.iuDarkGreyColor()
    
    byeLabel.translatesAutoresizingMaskIntoConstraints = false
    byeLabel.font = UIFont.iuBlackFont(24)
    byeLabel.text = Strings.Tour6ByeLabelText
    byeLabel.textColor = UIColor.iuLightBlueGreyColor()
    addSubview(byeLabel)
    
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
    logoImageView.image = UIImage.iuLogo()
    logoImageView.contentMode = .Center
    addSubview(logoImageView)
    
    labels = [byeLabel]
    images = [logoImageView]
    
    let byeConstraints = [
      NSLayoutConstraint(item: byeLabel, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: byeLabel, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    addConstraints(byeConstraints)
    let logoConstraints = [
      NSLayoutConstraint(item: logoImageView, attribute: .Right, relatedBy: .Equal, toItem: self, attribute: .RightMargin, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: logoImageView, attribute: .Bottom, relatedBy: .Equal, toItem: self, attribute: .Bottom, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: logoImageView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: Constants.navigationBarHeight)
    ]
    addConstraints(logoConstraints)
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    fadeIn(nil)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
