//
//  NewStudentView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/23/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol NewStudentViewDelegate: class {
  func newStudentView(newStudentView: NewStudentView, tappedNextWithName name: String, phone: Double)
}

final class NewStudentView: UIView {
  private let margin: CGFloat = 24
  private let textFieldPadding: CGFloat = 12
  private let hiddenAlpha: CGFloat = 0.5
  
  private let nameTextField = UITextField()
  private let phoneTextField = NBTextField()
  private let nextButton = UIButton(type: .Custom)
  
  weak var delegate: NewStudentViewDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    nameTextField.translatesAutoresizingMaskIntoConstraints = false
    nameTextField.placeholder = Strings.NewStudentTypeName
    nameTextField.font = UIFont.iuHeavyFont(16)
    nameTextField.textColor = UIColor.iuLightGreyColor()
    nameTextField.tintColor = UIColor.iuLightBlueGreyColor()
    nameTextField.backgroundColor = UIColor.whiteColor()
    nameTextField.autocapitalizationType = .Words
    nameTextField.layer.cornerRadius = 8
    nameTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    nameTextField.leftViewMode = .Always
    nameTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    nameTextField.rightViewMode = .Always
    nameTextField.delegate = self
    
    phoneTextField.translatesAutoresizingMaskIntoConstraints = false
    phoneTextField.placeholder = Strings.NewStudentTypePhone
    phoneTextField.font = UIFont.iuHeavyFont(16)
    phoneTextField.textColor = UIColor.iuLightGreyColor()
    phoneTextField.tintColor = UIColor.iuLightBlueGreyColor()
    phoneTextField.backgroundColor = UIColor.whiteColor()
    phoneTextField.keyboardType = .PhonePad
    phoneTextField.layer.cornerRadius = 8
    phoneTextField.countryCode = "BR"
    phoneTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    phoneTextField.leftViewMode = .Always
    phoneTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    phoneTextField.rightViewMode = .Always
    phoneTextField.delegate = self
    
    nextButton.translatesAutoresizingMaskIntoConstraints = false
    nextButton.backgroundColor = UIColor.iuUglyPinkColor()
    nextButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    nextButton.titleLabel?.font = UIFont.iuBlackFont(18)
    nextButton.setTitle(Strings.NewStudentNextButtonTitle.uppercaseString, forState: .Normal)
    nextButton.addTarget(self, action: #selector(NewStudentView.nextButtonPressed), forControlEvents: .TouchUpInside)
    nextButton.layer.cornerRadius = 8
    nextButton.enabled = false
    nextButton.alpha = hiddenAlpha
    
    let container = UIView()
    container.translatesAutoresizingMaskIntoConstraints = false
    container.addSubview(nameTextField)
    container.addSubview(phoneTextField)
    container.addSubview(nextButton)
    addSubview(container)
    
    let views = ["nameTextField": nameTextField, "phoneTextField": phoneTextField, "nextButton": nextButton, "container": container]
    let metrics = ["margin": margin, "margin2": margin * 3, "height": 64]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[nameTextField]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[phoneTextField]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[nextButton]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[nameTextField(height)]-margin-[phoneTextField(height)]-margin2-[nextButton(height)]|", options: [], metrics: metrics, views: views)
    container.addConstraints(verticalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: container, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0)]
    addConstraints(horizontalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: container, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0)]
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[container]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|->=0-[container]->=0-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    nameTextField.becomeFirstResponder()
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesEnded(touches, withEvent: event)
    endEditing(true)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Actions
extension NewStudentView {
  func nextButtonPressed() {
    endEditing(true)
    guard let name = nameTextField.text?.stripEndingSpaces() where name.characters.count > 0 else {
      return
    }
    guard let phone = phoneTextField.text where phone.characters.count > 9 else {
      return
    }
    let strippedPhone = phone.phoneStrip()
    guard strippedPhone.characters.count < 12, let doublePhone = Double(strippedPhone) else {
      AlertView.presentError(StudentError.InvalidPhone.description(), buttons: nil, inView: self)
      return
    }
    delegate?.newStudentView(self, tappedNextWithName: name, phone: doublePhone)
  }
}

// MARK: TextField Delegate
extension NewStudentView: UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    switch textField {
    case nameTextField:
      phoneTextField.becomeFirstResponder()
    default:
      phoneTextField.resignFirstResponder()
    }
    return true
  }
  
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    let startIndex = textField.text!.startIndex.advancedBy(range.location)
    let endIndex = startIndex.advancedBy(range.length)
    let indexRange = startIndex..<endIndex
    let newString = textField.text!.stringByReplacingCharactersInRange(indexRange, withString: string)
    let allFieldsFilled: Bool = {
      switch textField {
      case self.nameTextField:
        return newString.characters.count > 0 && phoneTextField.text?.characters.count > 0
      case self.phoneTextField:
        return nameTextField.text?.characters.count > 0 && newString.characters.count > 0
      default:
        return nameTextField.text?.characters.count > 0 && phoneTextField.text?.characters.count > 0
      }
    }()
    nextButton.enabled = allFieldsFilled
    nextButton.alpha = allFieldsFilled ? 1 : hiddenAlpha
    
    return true
  }
}
