//
//  PaymentTypeViewController.swift
//  iu.Professor
//
//  Created by Gilson Gil on 4/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class PaymentTypeViewController: UIViewController {
  private let paymentTypeView: PaymentTypeView
  private let newStudent: Bool
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(student: Student, newStudent: Bool) {
    paymentTypeView = PaymentTypeView(student: student)
    self.newStudent = newStudent
    super.init(nibName: nil, bundle: nil)
    paymentTypeView.delegate = self
  }
  
  override func loadView() {
    view = paymentTypeView
    AnswersHelper.pageView(Constants.answersPageViewBeforeAfter)
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension PaymentTypeViewController: PaymentTypeViewDelegate {
  func nextOnPaymentTypeView(studentViewModel: StudentViewModel) {
    let studentViewModel = StudentViewModel(studentViewModel: studentViewModel, newStudent: newStudent)
    let studentViewController = StudentViewController(studentViewModel: studentViewModel)
    navigationController?.pushViewController(studentViewController, animated: true)
  }
}
