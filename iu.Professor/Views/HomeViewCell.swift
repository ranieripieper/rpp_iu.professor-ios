//
//  HomeViewCell.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/23/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class HomeViewCell: UITableViewCell {
  private let margin: CGFloat = 26
  
  private let nameLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    accessoryType = .DisclosureIndicator
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.font = UIFont.iuBlackFont(20)
    nameLabel.textColor = UIColor.iuUglyPinkColor()
    contentView.addSubview(nameLabel)
    
    let metrics = ["margin": margin]
    let views = ["nameLabel": nameLabel]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[nameLabel]-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[nameLabel]-margin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Public
extension HomeViewCell {
  func update(viewModel: HomeViewCellViewModel) {
    nameLabel.text = viewModel.studentName
  }
}

// MARK: Updatable
extension HomeViewCell: Updatable {
  typealias ViewModel = HomeViewCellViewModel
}
