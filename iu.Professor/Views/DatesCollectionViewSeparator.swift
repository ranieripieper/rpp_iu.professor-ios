//
//  DatesCollectionViewSeparator.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/29/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class DatesCollectionViewSeparator: UICollectionReusableView {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    let label = UILabel(frame: frame)
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "-"
    label.textColor = UIColor.whiteColor()
    addSubview(label)
    let views = ["label": label]
    
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[label]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[label]|", options: [], metrics: nil, views: views)
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

final class DatesCollectionViewDarkSeparator: UICollectionReusableView {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    let label = UILabel(frame: frame)
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "-"
    label.textColor = UIColor.iuPinkishGreyColor()
    addSubview(label)
    let views = ["label": label]
    
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[label]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[label]|", options: [], metrics: nil, views: views)
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
