//
//  PulseImageView.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class PulseImageView: UIImageView {
  var shouldAnimate = true
  override var animationImages: [UIImage]? {
    didSet {
      image = animationImages?.first
    }
  }
  override var image: UIImage? {
    didSet {
      guard shouldAnimate else {
        return
      }
      openOut { [weak self] in
        self?.closeIn {
          self?.nextImage()
        }
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Private
private extension PulseImageView {
  func nextImage() {
    guard let animationImages = animationImages where animationImages.count > 0 else {
      return
    }
    guard let image = image else {
      self.image = animationImages.first
      return
    }
    guard let currentIndex = animationImages.indexOf(image) else {
      self.image = animationImages.first
      self.animationImages?.append(image)
      return
    }
    if currentIndex + 1 < animationImages.count {
      self.image = animationImages[currentIndex + 1]
    } else {
      self.image = animationImages.first
    }
  }
  
  func closeIn(completion: (() -> ())?) {
    guard shouldAnimate else {
      return
    }
    UIView.animateWithDuration(0.1, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: { [weak self] in
      self?.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1)
      }, completion: { _ in
        UIView.animateWithDuration(0.6, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: { [weak self] in
          self?.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001)
          }, completion: { _ in
            completion?()
        })
    })
  }
  
  func openOut(completion: (() -> ())?) {
    guard shouldAnimate else {
      return
    }
    UIView.animateWithDuration(0.6, delay: 0.1, usingSpringWithDamping: 0.7, initialSpringVelocity: 10, options: .CurveEaseOut, animations: { [weak self] in
      self?.transform = CGAffineTransformIdentity
      }, completion: { _ in
        completion?()
    })
  }
}
