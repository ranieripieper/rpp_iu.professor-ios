//
//  TourPage5View.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TourPage5View: UIView, TourPageViewProtocol {
  private let dateLabel = UILabel()
  private let calendarLabel = UILabel()
  private let detailImageView = UIImageView()
  private let selectedImageView = UIImageView()
  private let shadowImageView = UIImageView()
  private let studentLabel = UILabel()
  
  var labels: [UIView] = []
  var images: [UIView] = []
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.iuDarkGreyColor()
    
    dateLabel.translatesAutoresizingMaskIntoConstraints = false
    dateLabel.font = UIFont.iuBlackFont(36)
    dateLabel.text = Strings.Tour5DateLabelText
    dateLabel.textColor = UIColor.whiteColor()
    dateLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(dateLabel)
    
    calendarLabel.translatesAutoresizingMaskIntoConstraints = false
    calendarLabel.font = UIFont.iuBlackFont(14)
    calendarLabel.text = Strings.Tour5CalendarLabelText
    calendarLabel.textColor = UIColor.whiteColor()
    calendarLabel.textAlignment = .Center
    calendarLabel.numberOfLines = 0
    calendarLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(calendarLabel)
    
    detailImageView.translatesAutoresizingMaskIntoConstraints = false
    detailImageView.image = UIImage.iuCalendar()
    detailImageView.setContentCompressionResistancePriority(UILayoutPriorityDefaultHigh, forAxis: .Vertical)
    detailImageView.contentMode = .ScaleAspectFit
    addSubview(detailImageView)
    
    selectedImageView.translatesAutoresizingMaskIntoConstraints = false
    selectedImageView.image = UIImage.iuSelected()
    addSubview(selectedImageView)
    
    shadowImageView.translatesAutoresizingMaskIntoConstraints = false
    shadowImageView.image = UIImage.iuShadow()
    shadowImageView.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    shadowImageView.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(shadowImageView)
    
    studentLabel.translatesAutoresizingMaskIntoConstraints = false
    studentLabel.font = UIFont.iuBlackFont(14)
    studentLabel.text = Strings.Tour5StudentLabelText
    studentLabel.textColor = UIColor.whiteColor()
    studentLabel.textAlignment = .Center
    studentLabel.numberOfLines = 0
    studentLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(studentLabel)
    
    labels = [dateLabel, calendarLabel, studentLabel]
    images = [detailImageView, selectedImageView, shadowImageView]
    
    let metrics = ["margin": 20]
    let views = ["dateLabel": dateLabel, "calendarLabel": calendarLabel, "detailImageView": detailImageView, "selectedImageView": selectedImageView, "shadowImageView": shadowImageView, "studentLabel": studentLabel]
    [dateLabel, calendarLabel, detailImageView, shadowImageView, studentLabel].forEach {
      let horizontalConstraints = [
        NSLayoutConstraint(item: $0, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: $0, attribute: .Left, relatedBy: .GreaterThanOrEqual, toItem: self, attribute: .LeftMargin, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: $0, attribute: .Right, relatedBy: .LessThanOrEqual, toItem: self, attribute: .RightMargin, multiplier: 1, constant: 0)
      ]
      addConstraints(horizontalConstraints)
    }
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[dateLabel]-margin-[calendarLabel]-margin-[detailImageView]-[shadowImageView]-margin-[studentLabel]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    let centerConstraint = NSLayoutConstraint(item: detailImageView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: Constants.tourViewButtonHeight / 2)
    addConstraint(centerConstraint)
    
    let selectedConstraints = [
      NSLayoutConstraint(item: selectedImageView, attribute: .CenterX, relatedBy: .Equal, toItem: detailImageView, attribute: .CenterX, multiplier: 0.885, constant: 0),
      NSLayoutConstraint(item: selectedImageView, attribute: .CenterY, relatedBy: .Equal, toItem: detailImageView, attribute: .CenterY, multiplier: 0.96, constant: 0)
    ]
    addConstraints(selectedConstraints)
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    fadeIn { [weak self] in
      self?.goRight()
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Private
private extension TourPage5View {
  func goRight() {
    UIView.animateWithDuration(0.8, delay: 0.8, options: .CurveEaseInOut, animations: { [weak self] in
      self?.selectedImageView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 52, 0)
      }, completion: { [weak self] _ in
        self?.goLeft()
    })
  }
  
  func goLeft() {
    UIView.animateWithDuration(0.8, delay: 0.8, options: .CurveEaseInOut, animations: { [weak self] in
      self?.selectedImageView.transform = CGAffineTransformIdentity
      }, completion: { [weak self] _ in
        self?.goRight()
    })
  }
}
