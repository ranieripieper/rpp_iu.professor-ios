//
//  TourPage3View.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TourPage3View: UIView, TourPageViewProtocol {
  private let howLabel = UILabel()
  private let beforeAfterLabel = UILabel()
  private let deviceImageView = UIImageView()
  private let handImageView = UIImageView()
  private let shadowImageView = UIImageView()
  private let defineLabel = UILabel()
  
  var labels: [UIView] = []
  var images: [UIView] = []
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.iuDarkGreyColor()
    
    howLabel.translatesAutoresizingMaskIntoConstraints = false
    howLabel.font = UIFont.iuBlackFont(36)
    howLabel.text = Strings.Tour3HowLabelText
    howLabel.textColor = UIColor.whiteColor()
    howLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(howLabel)
    
    beforeAfterLabel.translatesAutoresizingMaskIntoConstraints = false
    beforeAfterLabel.font = UIFont.iuBlackFont(14)
    beforeAfterLabel.text = Strings.Tour3BeforeAfterLabelText
    beforeAfterLabel.textColor = UIColor.whiteColor()
    beforeAfterLabel.textAlignment = .Center
    beforeAfterLabel.adjustsFontSizeToFitWidth = true
    beforeAfterLabel.minimumScaleFactor = 0.5
    beforeAfterLabel.numberOfLines = 2
    beforeAfterLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(beforeAfterLabel)
    
    deviceImageView.translatesAutoresizingMaskIntoConstraints = false
    deviceImageView.image = UIImage.iuDeviceBefore()
    deviceImageView.setContentCompressionResistancePriority(UILayoutPriorityDefaultHigh, forAxis: .Vertical)
    deviceImageView.contentMode = .ScaleAspectFit
    addSubview(deviceImageView)
    
    handImageView.translatesAutoresizingMaskIntoConstraints = false
    handImageView.image = UIImage.iuHand()
    addSubview(handImageView)
    
    shadowImageView.translatesAutoresizingMaskIntoConstraints = false
    shadowImageView.image = UIImage.iuShadow()
    shadowImageView.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    shadowImageView.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(shadowImageView)
    
    defineLabel.translatesAutoresizingMaskIntoConstraints = false
    defineLabel.font = UIFont.iuBlackFont(14)
    defineLabel.text = Strings.Tour3DefineLabelText
    defineLabel.textColor = UIColor.whiteColor()
    defineLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    addSubview(defineLabel)
    
    labels = [howLabel, beforeAfterLabel, defineLabel]
    images = [deviceImageView, handImageView, shadowImageView]
    
    let metrics = ["margin": 20]
    let views = ["howLabel": howLabel, "beforeAfterLabel": beforeAfterLabel, "deviceImageView": deviceImageView, "handImageView": handImageView, "shadowImageView": shadowImageView, "defineLabel": defineLabel]
    [howLabel, beforeAfterLabel, defineLabel].forEach {
      let horizontalConstraints = [
        NSLayoutConstraint(item: $0, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: $0, attribute: .Left, relatedBy: .GreaterThanOrEqual, toItem: self, attribute: .LeftMargin, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: $0, attribute: .Right, relatedBy: .LessThanOrEqual, toItem: self, attribute: .RightMargin, multiplier: 1, constant: 0)
      ]
      addConstraints(horizontalConstraints)
    }
    let horizontalConstraints = [
      NSLayoutConstraint(item: handImageView, attribute: .Left, relatedBy: .Equal, toItem: self, attribute: .Left, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: deviceImageView, attribute: .Left, relatedBy: .Equal, toItem: handImageView, attribute: .Right, multiplier: 1, constant: -20),
      NSLayoutConstraint(item: shadowImageView, attribute: .CenterX, relatedBy: .Equal, toItem: deviceImageView, attribute: .CenterX, multiplier: 1, constant: 0)
    ]
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[howLabel]-margin-[beforeAfterLabel]-margin-[deviceImageView]-[shadowImageView]-margin-[defineLabel]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    let centerConstraint = NSLayoutConstraint(item: deviceImageView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: Constants.tourViewButtonHeight / 2)
    addConstraint(centerConstraint)
    let handVerticalConstraint = NSLayoutConstraint(item: handImageView, attribute: .CenterY, relatedBy: .Equal, toItem: deviceImageView, attribute: .CenterY, multiplier: 0.91, constant: 0)
    addConstraint(handVerticalConstraint)
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    fadeIn { [weak self] in
      self?.moveToAfter()
      if let weakSelf = self {
        weakSelf.images.append(weakSelf.handImageView)
      }
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Private
private extension TourPage3View {
  func moveToAfter() {
    UIView.animateWithDuration(1, delay: 1, options: .CurveEaseInOut, animations: { [weak self] in
      self?.handImageView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, 63)
      }, completion: { [weak self] _ in
        UIView.animateWithDuration(0.2, animations: {
          self?.deviceImageView.image = UIImage.iuDeviceAfter()
          }, completion: { _ in
            self?.moveToBefore()
        })
    })
  }
  
  func moveToBefore() {
    UIView.animateWithDuration(1, delay: 1, options: .CurveEaseInOut, animations: { [weak self] in
      self?.handImageView.transform = CGAffineTransformIdentity
      }, completion: { [weak self] _ in
        UIView.animateWithDuration(0.2, animations: {
          self?.deviceImageView.image = UIImage.iuDeviceBefore()
          }, completion: { _ in
            self?.moveToAfter()
        })
      })
  }
}
