//
//  TourButton.swift
//  iu.Professor
//
//  Created by Gilson Gil on 6/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

enum TourButtonState {
  case Next, Last, None
}

final class TourButton: UIButton {
  var tourState: TourButtonState = .None {
    didSet {
      switch tourState {
      case .Next:
        setTitle(Strings.TourNextButtonTitle, forState: .Normal)
        userInteractionEnabled = true
      case .Last:
        setTitle(Strings.TourLastButtonTitle, forState: .Normal)
        userInteractionEnabled = true
      case .None:
        setTitle(nil, forState: .Normal)
        userInteractionEnabled = false
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.iuAlmostWhiteColor()
    setTitleColor(UIColor.iuLightBlueGreyColor(), forState: .Normal)
    setTitleColor(UIColor.iuSeafoamBlueColor(), forState: .Highlighted)
    titleLabel?.font = UIFont.iuBlackFont(18)
  }
}
