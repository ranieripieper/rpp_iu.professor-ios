//
//  ClassesGivenCell.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ClassesGivenCell: UICollectionViewCell {
  private let dateLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.clearColor()
    
    dateLabel.translatesAutoresizingMaskIntoConstraints = false
    dateLabel.textAlignment = .Center
    dateLabel.font = UIFont.iuBlackFont(16)
    addSubview(dateLabel)
    
    let views = ["dateLabel": dateLabel]
    
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[dateLabel]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[dateLabel]|", options: [], metrics: nil, views: views)
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

// MARK: Public
extension ClassesGivenCell {
  func update(viewModel: ClassesGivenCellViewModel) {
    dateLabel.text = viewModel.dateString
    switch viewModel.style {
    case .White:
      dateLabel.textColor = UIColor.whiteColor()
    default:
      dateLabel.textColor = UIColor.iuPinkishGreyColor()
    }
  }
}

// MARK: Updatable
extension ClassesGivenCell: Updatable {
  typealias ViewModel = ClassesGivenCellViewModel
}
