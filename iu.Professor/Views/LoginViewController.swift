//
//  LoginViewController.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LoginViewController: UIViewController {
  weak var navigationDelegate: NavigationControllerDelegate?
  
  override func loadView() {
    let loginView = LoginView()
    loginView.delegate = self
    view = loginView
    AnswersHelper.pageView(Constants.answersPageViewLogin)
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension LoginViewController: LoginViewDelegate {
  func loginViewDidLogin(loginView: LoginView?) {
    navigationDelegate?.goToHome()
  }
  
  func signupButtonPressedOnLoginView(loginView: LoginView?) {
    let signupViewController = SignupViewController()
    signupViewController.navigationDelegate = navigationDelegate
    navigationController?.pushViewController(signupViewController, animated: true)
  }
}

extension LoginViewController: NavigationControllerProtocol {}
