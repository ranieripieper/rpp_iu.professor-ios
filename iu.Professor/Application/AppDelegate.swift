//
//  AppDelegate.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  var modelTransitioningDelegate: TourModelTransitioningDelegate?

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    #if RELEASE
      Fabric.with([Crashlytics.self])
//      FIRAppIndexing.sharedInstance().registerApp(1086122688)
    #endif
    ParseHelper.register()
    AppiraterHelper.register()
    
    window = UIWindow(frame: UIScreen.mainScreen().bounds)
    window?.makeKeyAndVisible()
    let navigationController = NavigationController()
    NSNotificationCenter.defaultCenter().addObserver(navigationController, selector: #selector(NavigationController.dropPro), name: Constants.dropProImageViewNotificationName, object: nil)
    window?.rootViewController = navigationController
    
    let shouldShowTour = !NSUserDefaults.standardUserDefaults().boolForKey(Constants.hasPresentedTourUserDefaultsKey)
    if shouldShowTour {
      showTour()
    } else {
      dropPro()
    }
    
    return true
  }
  
  func applicationWillEnterForeground(application: UIApplication) {
    AppiraterHelper.resume()
  }
  
  private func showTour() {
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.removeTour), name: Constants.tourDidFinishNotificationName, object: nil)
    let tourViewController = TourViewController()
    tourViewController.modalTransitionStyle = .CrossDissolve
    tourViewController.modalPresentationStyle = .OverFullScreen
    window?.rootViewController?.presentViewController(tourViewController, animated: false, completion: nil)
  }
  
  func removeTour() {
    NSNotificationCenter.defaultCenter().removeObserver(self, name: Constants.tourDidFinishNotificationName, object: nil)
    NSUserDefaults.standardUserDefaults().setBool(true, forKey: Constants.hasPresentedTourUserDefaultsKey)
    NSUserDefaults.standardUserDefaults().synchronize()
    modelTransitioningDelegate = TourModelTransitioningDelegate()
    window?.rootViewController?.presentedViewController?.transitioningDelegate = modelTransitioningDelegate!
    window?.rootViewController?.dismissViewControllerAnimated(true, completion: { [weak self] in
      self?.modelTransitioningDelegate = nil
      self?.dropPro()
    })
  }
  
  private func dropPro() {
    NSNotificationCenter.defaultCenter().postNotificationName(Constants.dropProImageViewNotificationName, object: nil)
  }
}
