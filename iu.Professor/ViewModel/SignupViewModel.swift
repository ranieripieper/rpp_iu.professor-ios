//
//  SignupViewModel.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct SignupViewModel {
  func signup(name: String?, email: String?, password: String?, passwordConfirmation: String?, completion: (inner: () throws -> ()) -> ()) {
    guard let name = name where name.characters.count > 0 else {
      return completion { throw SignupError.MissingName }
    }
    guard let email = email where email.characters.count > 0 else {
      return completion { throw SignupError.MissingEmail }
    }
    guard let password = password where password.characters.count > 0 else {
      return completion { throw SignupError.MissingPassword }
    }
    guard password == passwordConfirmation else {
      return completion { throw SignupError.MismatchingPasswords }
    }
    ParseHelper.signup(name, email: email, password: password) {
      do {
        try $0()
        completion { return }
      } catch {
        completion { throw error }
      }
    }
  }
}

enum SignupError: ErrorType {
  case Unhandled, ExistingEmail, MismatchingPasswords, MissingEmail, MissingName, MissingPassword
  
  func description() -> String {
    switch self {
    case .Unhandled:
      return "Erro desconhecido"
    case .ExistingEmail:
      return "Oh não! Já temos esse email cadastrado.\nPor favor, tente novamente."
    case .MismatchingPasswords:
      return "Ops! As senhas precisam ser iguais!\nPor favor, tente novamente."
    case .MissingName:
      return "Precisamos do seu nome!"
    case .MissingEmail:
      return "Precisamos do seu email!"
    case .MissingPassword:
      return "Precisamos de uma senha!"
    }
  }
}
