//
//  HomeViewModel.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct HomeViewModel {
  var students: [CellConfiguratorType]
  
  static func viewModel(completion: HomeViewModel -> ()) {
    ParseHelper.fetchStudents {
      let homeViewModel: HomeViewModel
      do {
        let students = try $0()
        let cellViewModels = students.map {
          HomeViewCellViewModel(student: $0)
        }
        let configurators: [CellConfiguratorType] = cellViewModels.flatMap {
          CellConfigurator<HomeViewCell>(viewModel: $0)
        }
        homeViewModel = HomeViewModel(students: configurators)
      } catch {
        homeViewModel = HomeViewModel(students: [])
      }
      completion(homeViewModel)
    }
  }
  
  func getStudent(phone: String, completion: (inner: () throws -> (Bool, [String: AnyObject]?)) -> ()) {

    ParseHelper.getStudentSummary(phone) { inner in
      do {
        let data = try inner()
        if let count = data["qt_classes"] as? Int where count != 0 {
          completion {return (true, data)}
        } else {
          completion {return (false, nil)}
        }
      } catch {
        completion {throw error}
      }
    }
  }
  
  func existingPhones() -> [Double] {
    let existingPhones = students.flatMap { item -> Double? in
      guard let viewModel = item.currentViewModel() as? HomeViewCellViewModel else {
        return nil
      }
      return viewModel.student["phone"] as? Double
    }
    return existingPhones
  }
  
  mutating func deleteStudentAtIndex(index: Int, completion: [CellConfiguratorType]? -> ()) {
    guard index < students.count else {
      completion(nil)
      return
    }
    guard let student = students[index].currentViewModel() as? HomeViewCellViewModel else {
      return
    }
    ParseHelper.deleteStudent(student.student) {
      self.students.removeAtIndex(index)
      completion(self.students)
    }
  }
}
