//
//  ClassesGivenCellViewModel.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct ClassesGivenCellViewModel {
  let dateString: String
  let style: DatesFlowLayoutStyle
  
  init(date: String, style: DatesFlowLayoutStyle) {
    dateString = date
    self.style = style
  }
}
