//
//  StudentViewModel.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/23/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Parse

enum StudentError: ErrorType {
  case Unhandled, MissingName, ShortName, MissingPhone, InvalidPhone, DateParameterMissing(Int, Student?)
  
  func description() -> String {
    switch self {
    case .Unhandled:
      return "Erro desconhecido"
    case .MissingName:
      return "Preencher Nome"
    case .MissingPhone:
      return "Preencher Telefone"
    case .InvalidPhone:
      return "Telefone inválido"
    case .ShortName:
      return "Nome deve ter pelo menos \(StudentViewModel.nameMinLength) caracteres"
    case .DateParameterMissing:
      return "É preciso enviar as datas de uso das aulas"
    }
  }
}

struct StudentViewModel {
  let student: Student
  let newStudent: Bool
  var paymentType: PaymentType
  var dateString: String {
    switch paymentType {
    case .Before(_, let expirationDate):
      return expirationDate
    case .After(_, let paymentDate):
      return paymentDate
    }
  }
  var classesGivenViewModel: ClassesGivenViewModel?
  
  init(student: Student) {
    self.student = student
    newStudent = false
    let dateFormatter = NSDateFormatter()
    dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    dateFormatter.dateFormat = "dd/MM/yyyy"
    paymentType = .Before(count: 0, expirationDate: dateFormatter.stringFromDate(NSDate()))
  }
  
  init(studentViewModel: StudentViewModel, newStudent: Bool) {
    student = studentViewModel.student
    self.newStudent = newStudent
    paymentType = studentViewModel.paymentType
    classesGivenViewModel = studentViewModel.classesGivenViewModel
  }
  
  init?(student: Student, data: [String: AnyObject]) {
    self.student = student
    self.newStudent = false
    guard let date = data["date"] as? NSDate, count = data["qt_classes"] as? Int, dates = data["recent_classes"] as? [PFObject], type = data["type"] as? Int else {
      return nil
    }
    let dateFormatter = NSDateFormatter()
    dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    dateFormatter.dateFormat = "dd/MM/yyyy"
    paymentType = type == 1 ? PaymentType.Before(count: count, expirationDate: dateFormatter.stringFromDate(date)) : PaymentType.After(count: count, paymentDate: dateFormatter.stringFromDate(date))
    dateFormatter.dateFormat = "dd/MM/yy"
    let classes = dates.flatMap { aClass -> String? in
      guard let aDate = aClass["usedAt"] as? NSDate else {
        return nil
      }
      return dateFormatter.stringFromDate(aDate)
    }
    classesGivenViewModel = ClassesGivenViewModel(classes: classes, style: DatesFlowLayoutStyle.Dark)
  }
  
  init(name: String, phone: Double, paymentType: PaymentType, newStudent: Bool) {
    student = Student()
    student["name"] = name
    student["phone"] = phone
    self.paymentType = paymentType
    self.newStudent = newStudent
  }
  
  init(student: Student, paymentType: PaymentType, newStudent: Bool) {
    self.student = student
    self.paymentType = paymentType
    self.newStudent = newStudent
  }
  
  private static func stringFromDate(date: NSDate) -> String {
    let dateFormatter = NSDateFormatter()
    dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    dateFormatter.dateFormat = "dd/MM/yyyy"
    return dateFormatter.stringFromDate(date)
  }
  
  static let nameMinLength = 1
  static let phoneRange = 10..<12
  
  func addStudent(completion: (inner: () throws -> Student) -> ()) {
    guard (student["name"] as? String ?? "").characters.count >= StudentViewModel.nameMinLength else {
      completion {throw StudentError.ShortName}
      return
    }
    let phoneString = String(format: "%.0f", arguments: [student["phone"] as? Double ?? 0])
    guard StudentViewModel.phoneRange.contains(phoneString.characters.count) else {
      completion {throw StudentError.InvalidPhone}
      return
    }
    
    ParseHelper.addStudent(student) { inner in
      do {
        let newStudent = try inner()
        completion { return newStudent }
      } catch {
        completion { throw error }
      }
    }
  }
  
  func saveStudent(classes: Int, expireDate: NSDate, usedDates: [NSDate]?, completion: (inner: () throws -> ()) -> ()) {
    let callback: Student? -> () = { newStudent in
      switch self.paymentType {
      case .Before(let count, _):
        guard let objectId = newStudent?.objectId ?? self.student.objectId else {
          return
        }
        let diff = classes - count
        if diff < 0 {
          completion { throw StudentError.DateParameterMissing(abs(diff), newStudent) }
        } else {
          ParseHelper.addClassesPayBefore(objectId, amount: diff, expirationDate: expireDate, completion: completion)
          if count == 0 {
            AnswersHelper.choseBefore()
          }
        }
      case .After(let count, _):
        let diff = classes - count
        guard let objectId = newStudent?.objectId ?? self.student.objectId else {
          return
        }
        if diff < 0 {
          ParseHelper.payAfterPay(objectId, amount: abs(diff), payedAt: expireDate, completion: completion)
        } else if diff == 0 {
          ParseHelper.payAfterUpdatePaymentDate(objectId, paymentDate: expireDate, completion: completion)
        } else {
          guard let usedDates = usedDates else {
            completion { throw StudentError.DateParameterMissing(diff, newStudent) }
            return
          }
          ParseHelper.addClassesPayAfter(objectId, paymentDate: expireDate, dates: usedDates, completion: completion)
          if count == 0 {
            AnswersHelper.choseAfter()
          }
        }
      }
    }
    
    if newStudent {
      addStudent() {
        do {
          let newStudent = try $0()
          callback(newStudent)
        } catch {
          completion {throw error}
        }
      }
    } else {
      callback(nil)
    }
  }
}
