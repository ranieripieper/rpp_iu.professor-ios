//
//  LoginViewModel.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct LoginViewModel {
  func login(email: String?, password: String?, completion: (inner: () throws -> ()) -> ()) {
    guard let email = email where email.characters.count > 0 else {
      completion { throw LoginError.MissingEmail }
      return
    }
    guard let password = password else {
      completion { throw LoginError.MissingPassword }
      return
    }
    
    ParseHelper.login(email, password: password) {
      do {
        try $0()
        completion { return }
      } catch {
        completion { throw error }
      }
    }
  }
  
  func forgotPassword(email: String?, completion: (inner: () throws -> ()) -> ()) {
    guard let email = email where email.characters.count > 0 else {
      completion { throw LoginError.MissingEmail }
      return
    }
    ParseHelper.forgotPassword(email, completion: completion)
  }
}

enum LoginError: ErrorType {
  case Unhandled, MissingEmail, MissingPassword, EmailNotFound, EmailSent
  
  func description() -> String {
    switch self {
    case .Unhandled:
      return "Erro desconhecido"
    case .MissingEmail:
      return "Precisamos do seu email\npara poder te ajudar!"
    case .MissingPassword:
      return "Por favor, preencha sua senha."
    case .EmailNotFound:
      return "Não encontramos esse email em nosso sistema"
    case .EmailSent:
      return "Mandaremos um email para você!"
    }
  }
}
