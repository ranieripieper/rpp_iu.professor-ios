//
//  UseClassesViewModel.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/23/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct UseClassesViewModel {
  let studentViewModel: StudentViewModel
  let expireDate: NSDate
  let classesCount: Int
  let useClassesCount: Int
  var selectedDates = [NSDate]()
  var selectedDatesStrings: [String] {
    let dateFormatter = NSDateFormatter()
    dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    dateFormatter.dateFormat = "dd/MM/yy"
    return selectedDates.flatMap {
      return dateFormatter.stringFromDate($0)
    }
  }
  
  init(studentViewModel: StudentViewModel, expireDate: NSDate, classesCount: Int, useClassesCount: Int) {
    self.studentViewModel = studentViewModel
    self.expireDate = expireDate
    self.classesCount = classesCount
    self.useClassesCount = useClassesCount
  }
  
  func useClasses(completion: (inner: () throws -> ()) -> ()) {
    guard let objectId = studentViewModel.student.objectId else {
      return
    }
    switch studentViewModel.paymentType {
    case .Before:
      ParseHelper.payBeforeCompleteClasses(objectId, dates: selectedDates, date: expireDate, completion: completion)
    case .After:
      ParseHelper.addClassesPayAfter(objectId, paymentDate: expireDate, dates: selectedDates, completion: completion)
    }
  }
}
