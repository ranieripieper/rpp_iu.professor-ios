//
//  AllClassesGivenViewModel.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/30/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct AllClassesGivenViewModel {
  let student: Student
  let months: [CellConfiguratorType]
  let currentDates: [NSDate]
  
  static var monthDateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    dateFormatter.dateFormat = "MMMM yyyy"
    return dateFormatter
  }()
  
  static var fullDateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    dateFormatter.dateFormat = "dd/MM/yy"
    return dateFormatter
  }()
  
  init(student: Student) {
    self.student = student
    months = []
    currentDates = []
  }
  
  init(student: Student, months: [CellConfiguratorType], currentDates: [NSDate]) {
    self.student = student
    self.months = months
    self.currentDates = currentDates
  }
  
  func load(page: Int, completion: (inner: () throws -> AllClassesGivenViewModel) -> ()) {
    guard let phone = student["phone"] as? Double else {
      completion { throw ParseError.Unhandled }
      return
    }
    ParseHelper.givenClasses(phone, page: page) { inner in
      do {
        let dates = try inner()
        guard dates.count > 0 else {
          completion { throw ParseError.Unhandled }
          return
        }
        let allDates = self.currentDates + dates
        let groupedMonths: [[NSDate]] = allDates.groupMonths() ?? []
        let months = groupedMonths.map { month -> CellConfiguratorType in
          return CellConfigurator<AllClassesGivenCell>(viewModel: AllClassesGivenCellViewModel(month: AllClassesGivenViewModel.monthDateFormatter.stringFromDate(month.first!), dates: month.map { date -> CellConfiguratorType in
            return CellConfigurator<ClassesGivenCell>(viewModel: ClassesGivenCellViewModel(date: AllClassesGivenViewModel.fullDateFormatter.stringFromDate(date), style: .Dark))
            }))
        }
        let newAllClassesGivenViewModel = AllClassesGivenViewModel(student: self.student, months: months, currentDates: allDates)
        completion { return newAllClassesGivenViewModel }
      } catch {
        completion { throw ParseError.Unhandled }
      }
    }
  }
}

extension Array {
  func groupMonths() -> [[NSDate]]? {
    let dates = self.flatMap { date -> NSDate? in
      guard let date = date as? NSDate else {
        return nil
      }
      return date
    }
    var groupedMonths = [[NSDate]]()
    let currentCalendar = NSCalendar.currentCalendar()
    let years = dates.map { date -> Int in
      let components = currentCalendar.components([.Year], fromDate: date)
      return components.year
    }
    let yearsSet = Set(years).sort { $0 > $1 }
    yearsSet.forEach { year in
      let filteredYear = dates.filter { date in
        let components = currentCalendar.components([.Year], fromDate: date)
        return components.year == year
      }
      let months = filteredYear.flatMap { date -> Int in
        let components = currentCalendar.components([.Month], fromDate: date)
        return components.month
      }
      let monthsSet = Set(months).sort { $0 > $1 }
      monthsSet.forEach { month in
        let filteredMonth = filteredYear.filter { date in
          let components = currentCalendar.components([.Month], fromDate: date)
          return components.month == month
        }
        groupedMonths.append(filteredMonth)
      }
    }
    return groupedMonths
  }
}
