//
//  HomeViewCellViewModel.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/23/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct HomeViewCellViewModel {
  let student: Student
  var studentName: String {
    return student["name"] as? String ?? ""
  }
}
