//
//  PaymentTypeViewModel.swift
//  iu.Professor
//
//  Created by Gilson Gil on 4/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

enum PaymentType {
  case Before(count: Int, expirationDate: String),
       After(count: Int, paymentDate: String)
}

struct PaymentTypeViewModel {
  let student: Student
  var studentName: String {
    return student["name"] as? String ?? ""
  }
  
  func studentViewModel(before: Bool) -> StudentViewModel {
    let dateFormatter = NSDateFormatter()
    dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    dateFormatter.dateFormat = "dd/MM/yyyy"
    let today = dateFormatter.stringFromDate(NSDate())
    
    let studentViewModel = StudentViewModel(student: student, paymentType: before ? .Before(count: 0, expirationDate: today) : .After(count: 0, paymentDate: today), newStudent: false)
    return studentViewModel
  }
}
