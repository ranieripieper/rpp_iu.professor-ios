//
//  ClassesGivenViewModel.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct ClassesGivenViewModel {
  let classes: [CellConfiguratorType]
  
  init(classes: [ClassesGivenCellViewModel]) {
    self.classes = classes.map {
      CellConfigurator<ClassesGivenCell>(viewModel: $0)
    }
  }
  
  init?(classes: [CellConfiguratorType]) {
    guard classes.first?.cellClass == ClassesGivenCell.classForCoder() else {
      return nil
    }
    self.classes = classes
  }
  
  init(classes: [String], style: DatesFlowLayoutStyle) {
    let cellViewModels = classes.map {
      ClassesGivenCellViewModel(date: $0, style: style)
    }
    self.init(classes: cellViewModels)
  }
}
